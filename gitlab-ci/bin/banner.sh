#!/bin/sh

. gitlab-ci/bin/colors.sh

set -eu

PAIR="%16s: %s"
printf "${COLOR_PURPLE}${PAIR}\n${PAIR}\n${PAIR}\n${PAIR}\n${PAIR}\n${COLOR_NONE}" \
    "Commit Ref" "$CI_COMMIT_REF_NAME" \
    "Build" "${BUILD_VERSION:-(NOT SET)}" \
    "Pipeline" "$CI_PIPELINE_NAME" \
    "Job" "$CI_JOB_NAME" \
    "ci.yml" "${CI_PROJECT_URL}/-/blob/${CI_COMMIT_SHA}/${SUB_PROJECT}/ci.yml" \
    ;

