#!/bin/bash
#
# Overwrite all `version.py` files under version control with a doscstring containing the current date and
# time and assigning the string `$BUILD_VERSION` to `__version__`

set -euo pipefail

DOCSTRING="$( echo -e "'''\nBuilt at: $( date +%F-%T )\n'''" )"
VERSION_PY="__version__ = '${BUILD_VERSION}'"
echo -e "writing \"${VERSION_PY}\" in..."
git ls-files **/version.py | while read p ; do echo "  $p"; echo "${DOCSTRING}" >"$p"; echo "${VERSION_PY}" >>"$p"; done

