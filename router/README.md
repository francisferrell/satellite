
# Satellite Routers

All applications connect via websocket to a router. Routers provide persistence of sessions, and route
messages on behalf of their connected users.

