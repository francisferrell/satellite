
variable "name" {
  type = string
  description = "The name of this instance of satellite."
}

variable "description" {
  type = string
  description = "The description of this instance of satellite."
  default = "Satellite websocket API bus"
}

variable "domain_name" {
  type = string
  description = "The custom domain name to associate with the created API Gateway stage."
  default = null
}

variable "domain_name_certificate_arn" {
  type = string
  description = "The ARN of a certificate corresponding to `domain_name`."
  default = null
}

variable "environment_variables" {
  type = map(string)
  description = "Extra environment variables to assign to the lambda."
  default = {}
}

