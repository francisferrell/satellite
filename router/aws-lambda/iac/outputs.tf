
output "api_id" {
  value = module.apigw.api_id
  description = "The API Gateway ID"
}

output "api_arn" {
  value = module.apigw.api_arn
  description = "The API Gateway ARN"
}

output "api_endpoint" {
  value = module.apigw.api_endpoint
  description = "The API Gateway connection endpoint"
}

output "api_target_domain_name" {
  value = module.apigw.domain_name_target_domain_name
  description = "The API Gateway target domain name for the stage. Set `domain_name` to this in your DNS."
}

output "dynamodb_table" {
  value = module.dynamodb.dynamodb_table_id
  description = "The DynamoDB table name"
}

output "lambda_function_arn" {
  value = module.lambda.lambda_function_arn
  description = "The Lambda function ARN"
}

output "lambda_function_name" {
  value = module.lambda.lambda_function_name
  description = "The Lambda function name"
}

