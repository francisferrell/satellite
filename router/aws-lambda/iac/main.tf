/**
 * Satellite AWS Lambda + DynamoDB + API Gateway
 *
 * This module provides an instance of the Satellite Router for AWS Lambda.
 */

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 5.0.0, < 6.0.0"
    }
  }
}



module "apigw" {
  source = "terraform-aws-modules/apigateway-v2/aws"
  version = "5.2.0"

  name = var.name
  description = var.description
  protocol_type = "WEBSOCKET"
  deploy_stage = true

  create_domain_name = var.domain_name == null ? false : true
  create_certificate = false
  create_domain_records = false

  domain_name = var.domain_name
  domain_name_certificate_arn = var.domain_name_certificate_arn

  routes = {
    "$connect" = {
      operation_name = "ConnectRoute"
      integration = {
        uri = module.lambda.lambda_function_invoke_arn
      }
    },
    "$disconnect" = {
      operation_name = "DisconnectRoute"
      integration = {
        uri = module.lambda.lambda_function_invoke_arn
      }
    },
    "$default" = {
      operation_name = "DefaultRoute"
      integration = {
        uri = module.lambda.lambda_function_invoke_arn
      }
      route_response = {
        create = true
      }
    },
  }

  stage_default_route_settings = {
    logging_level = "INFO"
  }

  stage_access_log_settings = {
    create_log_group = true
    log_group_retention_in_days = 7
    format = jsonencode( {
      domainName = "$context.domainName"
      requestId = "$context.requestId"
      requestTime = "$context.requestTime"
      routeKey = "$context.routeKey"
      stage = "$context.stage"
      status = "$context.status"
      error = {
        message = "$context.error.message"
        responseType = "$context.error.responseType"
      }
      identity = {
        sourceIP = "$context.identity.sourceIp"
      }
      integration = {
        error = "$context.integration.error"
        integrationStatus = "$context.integration.integrationStatus"
      }
    } )
  }
}



module "dynamodb" {
  source = "terraform-aws-modules/dynamodb-table/aws"
  version = "4.2.0"

  name = var.name
  attributes = [
    {
      name = "pk"
      type = "S"
    },
    {
      name = "sk"
      type = "S"
    },
  ]
  hash_key = "pk"
  range_key = "sk"
  global_secondary_indexes = [
    {
      name = "SID"
      hash_key = "sk"
      range_key = "pk"
      projection_type = "ALL"
    }
  ]
  server_side_encryption_enabled = true
  table_class = "STANDARD"
}



module "lambda" {
  source = "terraform-aws-modules/lambda/aws"
  version = "7.14.0"

  function_name = var.name
  description = var.description
  handler = "satellite_lambda.main.handler"
  runtime = "python3.12"
  architectures = ["arm64"]
  memory_size = 512
  timeout = 15

  environment_variables = merge(
    var.environment_variables,
    {
      CONF_DB__TABLE_NAME = module.dynamodb.dynamodb_table_id
    },
  )

  cloudwatch_logs_retention_in_days = 30
  attach_create_log_group_permission = false

  create_package = false
  local_existing_package = "${path.module}/assets/dummy.zip"
  ignore_source_code_hash = true

  create_current_version_allowed_triggers = false
  allowed_triggers = {
    APIGatewayAny = {
      service = "apigateway"
      source_arn = "${module.apigw.api_execution_arn}/*"
    }
  }

  attach_policy_json = true
  policy_json = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ApiGwWebsocketManagement"
        Effect = "Allow"
        Action = [
          "execute-api:ManageConnections",
        ]
        Resource = [
          "${module.apigw.api_execution_arn}/*",
        ]
      },
      {
        Sid = "DynamodbTableAccess"
        Effect = "Allow"
        Action = [
          "dynamodb:*Item",
          "dynamodb:DescribeTable",
          "dynamodb:ListTagsOfResource",
          "dynamodb:PartiQL*",
          "dynamodb:Query",
          "dynamodb:Scan",
        ]
        Resource = [
            module.dynamodb.dynamodb_table_arn,
        ]
      },
      {
        Sid = "DynamodbTableIndexAccess"
        Effect = "Allow"
        Action = [
          "dynamodb:PartiQL*",
          "dynamodb:Query",
          "dynamodb:Scan",
        ]
        Resource = [
            "${module.dynamodb.dynamodb_table_arn}/index/*",
        ]
      },
    ]
  } )
}

