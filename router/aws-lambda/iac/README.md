Satellite AWS Lambda + DynamoDB + API Gateway

This module provides an instance of the Satellite Router for AWS Lambda.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.0.0, < 6.0.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_apigw"></a> [apigw](#module\_apigw) | terraform-aws-modules/apigateway-v2/aws | 5.2.0 |
| <a name="module_dynamodb"></a> [dynamodb](#module\_dynamodb) | terraform-aws-modules/dynamodb-table/aws | 4.2.0 |
| <a name="module_lambda"></a> [lambda](#module\_lambda) | terraform-aws-modules/lambda/aws | 7.14.0 |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | The description of this instance of satellite. | `string` | `"Satellite websocket API bus"` | no |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | The custom domain name to associate with the created API Gateway stage. | `string` | `null` | no |
| <a name="input_domain_name_certificate_arn"></a> [domain\_name\_certificate\_arn](#input\_domain\_name\_certificate\_arn) | The ARN of a certificate corresponding to `domain_name`. | `string` | `null` | no |
| <a name="input_environment_variables"></a> [environment\_variables](#input\_environment\_variables) | Extra environment variables to assign to the lambda. | `map(string)` | `{}` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of this instance of satellite. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_api_arn"></a> [api\_arn](#output\_api\_arn) | The API Gateway ARN |
| <a name="output_api_endpoint"></a> [api\_endpoint](#output\_api\_endpoint) | The API Gateway connection endpoint |
| <a name="output_api_id"></a> [api\_id](#output\_api\_id) | The API Gateway ID |
| <a name="output_api_target_domain_name"></a> [api\_target\_domain\_name](#output\_api\_target\_domain\_name) | The API Gateway target domain name for the stage. Set `domain_name` to this in your DNS. |
| <a name="output_dynamodb_table"></a> [dynamodb\_table](#output\_dynamodb\_table) | The DynamoDB table name |
| <a name="output_lambda_function_arn"></a> [lambda\_function\_arn](#output\_lambda\_function\_arn) | The Lambda function ARN |
| <a name="output_lambda_function_name"></a> [lambda\_function\_name](#output\_lambda\_function\_name) | The Lambda function name |
