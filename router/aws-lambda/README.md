
# Satellite Router for AWS Lambda

This router provides a serverless implementation. It uses API Gateway for websocket connectivity, Lambda for
message handling, and DynamoDB for persistence of state while the router is idle.

As a result, it can operate at very low cost, potentially even free. As of this writing, Lambda and DynamoDB
offer a permanent free tier while API Gateway offers a 12-month free tier. As with all things AWS pricing,
the devils in the details are *numerous*.


## Limitations

Lambda cold starts will affect overall system performance.

API Gateway imposes a 29 second timeout on Lambda integration. This puts an upper bound on how many of a given
event can be sent to the subscribers of a topic.

