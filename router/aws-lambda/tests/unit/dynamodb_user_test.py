# mypy: allow-untyped-defs

import pytest

from satellite import exceptions
from satellite_lambda import dynamodb



@pytest.mark.asyncio
async def test_user( alice ):
    store = dynamodb.DynamoDBStore()
    assert alice.realm in alice.pk
    assert alice.name in alice.pk
    retrieved_user = await store.get_user( alice.realm, alice.name )
    assert retrieved_user == alice



@pytest.mark.asyncio
async def test_user_missing( dynamodb_table, alice ):
    store = dynamodb.DynamoDBStore()
    dynamodb_table.delete_item(
        Key = {
            'pk': alice.pk,
            'sk': 'profile'
        }
    )
    with pytest.raises( exceptions.MissingUser ):
        await store.get_user( alice.realm, alice.name )



@pytest.mark.asyncio
async def test_user_across_realms( helpers, dynamodb_table, realm1, realm2 ):
    store = dynamodb.DynamoDBStore()
    name = 'foo'
    helpers.create_user( dynamodb_table, realm1, name )
    helpers.create_user( dynamodb_table, realm2, name )

    foo1 = await store.get_user( realm1, name )
    foo2 = await store.get_user( realm2, name )

    assert foo1.name == name
    assert foo1.realm == realm1
    assert foo2.name == name
    assert foo2.realm == realm2
    assert foo1 != foo2

    dynamodb_table.delete_item(
        Key = {
            'pk': foo1.pk,
            'sk': 'profile'
        }
    )

    with pytest.raises( exceptions.MissingUser ):
        await store.get_user( realm1, name )
    assert foo2 == await store.get_user( realm2, name )

