# mypy: allow-untyped-defs

from copy import deepcopy

from boto3.dynamodb.conditions import Key
import pytest
from satellite import common, exceptions

from satellite_lambda import dynamodb, main



@pytest.fixture
def apigw_connect_event_custom_dns_json( apigw_connect_event_json ):
    custom_dns = 'satellite.example.com'
    event_json = deepcopy( apigw_connect_event_json )
    event_json['headers']['Host'] = custom_dns
    event_json['multiValueHeaders']['Host'][0] = custom_dns
    event_json['requestContext']['domainName'] = custom_dns
    return event_json

@pytest.fixture
def apigw_disconnect_event_custom_dns_json( apigw_disconnect_event_json ):
    custom_dns = 'satellite.example.com'
    event_json = deepcopy( apigw_disconnect_event_json )
    event_json['headers']['Host'] = custom_dns
    event_json['multiValueHeaders']['Host'][0] = custom_dns
    event_json['requestContext']['domainName'] = custom_dns
    return event_json



def test_handle_meta():
    event = { "satelliteMeta": "warm" }
    response = main.handler( event, None )
    assert response['statusCode'] == 200



@pytest.mark.asyncio
async def test_handle_connect( dynamodb_table, apigw_connect_event_json ):
    domain = apigw_connect_event_json['requestContext']['domainName']
    stage = apigw_connect_event_json['requestContext']['stage']
    connection_id = apigw_connect_event_json['requestContext']['connectionId']

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}/{stage}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' not in response

    response = await main.handle_apigw_ws_event( apigw_connect_event_json )
    assert response['statusCode'] == 200

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}/{stage}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' in response



@pytest.mark.asyncio
async def test_handle_connect_custom_dns( dynamodb_table, apigw_connect_event_custom_dns_json ):
    domain = apigw_connect_event_custom_dns_json['requestContext']['domainName']
    connection_id = apigw_connect_event_custom_dns_json['requestContext']['connectionId']

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' not in response

    response = await main.handle_apigw_ws_event( apigw_connect_event_custom_dns_json )
    assert response['statusCode'] == 200

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' in response



@pytest.mark.asyncio
async def test_handle_disconnect( helpers, dynamodb_table, apigw_disconnect_event_json ):
    domain = apigw_disconnect_event_json['requestContext']['domainName']
    stage = apigw_disconnect_event_json['requestContext']['stage']
    connection_id = apigw_disconnect_event_json['requestContext']['connectionId']
    session = dynamodb.APIGatewayAppSession(
        realm = f'{domain}/{stage}',
        session_id = connection_id,
        role = 'anonymous',
    )
    topic_uri = 'foo.one'
    rpc_uri = 'foo.two'
    call_uri = 'foo.three'
    router_ref = helpers.uuid()
    caller_ref = helpers.uuid()

    await main.get_dynamodb_store().put_session( session )

    await main.get_dynamodb_store().put_subscription( session, topic_uri )
    assert await main.get_dynamodb_store().get_subscribers( session.realm, topic_uri ) == [ session ]

    await main.get_dynamodb_store().put_registration( session, rpc_uri )
    assert await main.get_dynamodb_store().get_registrant( session.realm, rpc_uri ) == session

    await main.get_dynamodb_store().put_call( session, call_uri, router_ref, caller_ref )
    call_context = await main.get_dynamodb_store().get_call( session.realm, call_uri, router_ref )
    assert call_context.caller == session

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}/{stage}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' in response

    response = await main.handle_apigw_ws_event( apigw_disconnect_event_json )
    assert response['statusCode'] == 200

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}/{stage}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' not in response

    assert await main.get_dynamodb_store().get_subscribers( session.realm, topic_uri ) == []
    with pytest.raises( exceptions.MissingRegistration ):
        await main.get_dynamodb_store().get_registrant( session.realm, rpc_uri )
    with pytest.raises( exceptions.MissingInvocation ):
        await main.get_dynamodb_store().get_call( session.realm, call_uri, router_ref )



@pytest.mark.asyncio
async def test_handle_disconnect_custom_dns( helpers, dynamodb_table, apigw_disconnect_event_custom_dns_json ):
    domain = apigw_disconnect_event_custom_dns_json['requestContext']['domainName']
    connection_id = apigw_disconnect_event_custom_dns_json['requestContext']['connectionId']

    session = dynamodb.APIGatewayAppSession(
        realm = domain,
        session_id = connection_id,
        role = 'anonymous',
    )
    topic_uri = 'foo.one'
    rpc_uri = 'foo.two'
    call_uri = 'foo.three'
    router_ref = helpers.uuid()
    caller_ref = helpers.uuid()

    await main.get_dynamodb_store().put_session( session )

    await main.get_dynamodb_store().put_subscription( session, topic_uri )
    assert await main.get_dynamodb_store().get_subscribers( session.realm, topic_uri ) == [ session ]

    await main.get_dynamodb_store().put_registration( session, rpc_uri )
    assert await main.get_dynamodb_store().get_registrant( session.realm, rpc_uri ) == session

    await main.get_dynamodb_store().put_call( session, call_uri, router_ref, caller_ref )
    call_context = await main.get_dynamodb_store().get_call( session.realm, call_uri, router_ref )
    assert call_context.caller == session

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' in response

    response = await main.handle_apigw_ws_event( apigw_disconnect_event_custom_dns_json )
    assert response['statusCode'] == 200

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}#{connection_id}',
            'sk': '_',
        }
    )
    assert 'Item' not in response

    assert await main.get_dynamodb_store().get_subscribers( session.realm, topic_uri ) == []
    with pytest.raises( exceptions.MissingRegistration ):
        await main.get_dynamodb_store().get_registrant( session.realm, rpc_uri )
    with pytest.raises( exceptions.MissingInvocation ):
        await main.get_dynamodb_store().get_call( session.realm, call_uri, router_ref )



@pytest.mark.asyncio
async def test_handle_message( dynamodb_table, uri, apigw_message_event_json ):
    domain = apigw_message_event_json['requestContext']['domainName']
    stage = apigw_message_event_json['requestContext']['stage']
    connection_id = apigw_message_event_json['requestContext']['connectionId']
    session = dynamodb.APIGatewayAppSession(
        realm = f'{domain}/{stage}',
        session_id = connection_id,
        role = 'anonymous',
    )

    await main.get_dynamodb_store().put_session( session )

    request = common.SubscribeRequest( uri = uri )
    apigw_message_event_json['body'] = request.tojson()
    response = await main.handle_apigw_ws_event( apigw_message_event_json )
    assert response['statusCode'] == 200

    response = dynamodb_table.get_item(
        Key = {
            'pk': f'Session#{domain}/{stage}#{connection_id}',
            'sk': f'Subscription#{uri}',
        }
    )
    assert 'Item'in response

