# mypy: allow-untyped-defs

import pytest

from satellite import exceptions
from satellite_lambda import dynamodb



@pytest.mark.asyncio
async def test_put_session( helpers, dynamodb_table, realm ):
    store = dynamodb.DynamoDBStore()
    session = dynamodb.APIGatewayAppSession(
        realm = realm,
        session_id = helpers.uuid(),
        role = 'anonymous',
    )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': '_',
        }
    )
    assert 'Item' not in response

    await store.put_session( session )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': '_',
        }
    )
    assert 'Item' in response



@pytest.mark.asyncio
async def test_delete_session( helpers, dynamodb_table, realm ):
    store = dynamodb.DynamoDBStore()
    session = dynamodb.APIGatewayAppSession(
        realm = realm,
        session_id = helpers.uuid(),
        role = 'anonymous',
    )
    topic_uri = 'foo.one'
    rpc_uri = 'foo.two'
    call_uri = 'foo.three'
    router_ref = helpers.uuid()
    caller_ref = helpers.uuid()

    await store.put_session( session )

    await store.put_subscription( session, topic_uri )
    assert await store.get_subscribers( session.realm, topic_uri ) == [ session ]

    await store.put_registration( session, rpc_uri )
    assert await store.get_registrant( session.realm, rpc_uri ) == session

    await store.put_call( session, call_uri, router_ref, caller_ref )
    call_context = await store.get_call( session.realm, call_uri, router_ref )
    assert call_context.caller == session

    await store.delete_session( session )

    assert await store.get_subscribers( session.realm, topic_uri ) == []
    with pytest.raises( exceptions.MissingRegistration ):
        await store.get_registrant( session.realm, rpc_uri )
    with pytest.raises( exceptions.MissingInvocation ):
        await store.get_call( session.realm, call_uri, router_ref )

