# mypy: allow-untyped-defs

import pytest

from satellite import exceptions
from satellite_lambda import dynamodb



@pytest.mark.asyncio
async def test_put_registration( helpers, dynamodb_table, alice, uri ):
    store = dynamodb.DynamoDBStore()
    session = helpers.create_session( dynamodb_table, alice )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': f'Registration#{uri}',
        }
    )
    assert 'Item' not in response
    with pytest.raises( exceptions.MissingRegistration ):
        await store.get_registrant( session.realm, uri )

    await store.put_registration( session, uri )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': f'Registration#{uri}',
        }
    )
    assert 'Item' in response
    assert await store.get_registrant( session.realm, uri ) == session



@pytest.mark.asyncio
async def test_get_registrant_empty( helpers, realm, uri ):
    store = dynamodb.DynamoDBStore()
    with pytest.raises( exceptions.MissingRegistration ):
        await store.get_registrant( realm, uri )



@pytest.mark.asyncio
async def test_get_registrant_cross_realm( dynamodb_table, helpers, realm1, realm2, uri ):
    store = dynamodb.DynamoDBStore()
    user1 = helpers.create_user( dynamodb_table, realm1, 'bob' )
    session1 = helpers.create_session( dynamodb_table, user1 )
    user2 = helpers.create_user( dynamodb_table, realm2, 'bob' )
    session2 = helpers.create_session( dynamodb_table, user2 )

    await store.put_registration( session1, uri )
    await store.put_registration( session2, uri )

    assert session1 == await store.get_registrant( realm1, uri )
    assert session2 == await store.get_registrant( realm2, uri )

