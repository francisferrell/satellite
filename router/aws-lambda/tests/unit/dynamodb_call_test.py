# mypy: allow-untyped-defs

import pytest

from satellite import exceptions
from satellite_lambda import dynamodb



@pytest.mark.asyncio
async def test_put_call( helpers, dynamodb_table, alice, uri ):
    store = dynamodb.DynamoDBStore()
    session = helpers.create_session( dynamodb_table, alice )
    router_ref = helpers.uuid()
    caller_ref = helpers.uuid()

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': f'Call#{uri}#{router_ref}',
        }
    )
    assert 'Item' not in response
    with pytest.raises( exceptions.MissingInvocation ):
        await store.get_call( session.realm, uri, router_ref )

    await store.put_call( session, uri, router_ref, caller_ref )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': f'Call#{uri}#{router_ref}',
        }
    )
    assert 'Item' in response
    call_context = await store.get_call( session.realm, uri, router_ref )
    assert call_context.caller == session
    assert call_context.caller_ref == caller_ref



@pytest.mark.asyncio
async def test_get_call_empty( helpers, realm, uri ):
    store = dynamodb.DynamoDBStore()
    with pytest.raises( exceptions.MissingInvocation ):
        await store.get_call( realm, uri, 'foo' )



@pytest.mark.asyncio
async def test_get_call_multiple( dynamodb_table, helpers, realm, uri ):
    store = dynamodb.DynamoDBStore()
    user1 = helpers.create_user( dynamodb_table, realm, 'user1' )
    session1 = helpers.create_session( dynamodb_table, user1 )
    router_ref1 = helpers.uuid()
    caller_ref1 = helpers.uuid()
    user2 = helpers.create_user( dynamodb_table, realm, 'user2' )
    session2 = helpers.create_session( dynamodb_table, user2 )
    router_ref2 = helpers.uuid()
    caller_ref2 = helpers.uuid()

    await store.put_call( session1, uri, router_ref1, caller_ref1 )
    await store.put_call( session2, uri, router_ref2, caller_ref2 )

    call_context1 = await store.get_call( session1.realm, uri, router_ref1 )
    assert call_context1.caller == session1
    assert call_context1.caller_ref == caller_ref1
    call_context2 = await store.get_call( session2.realm, uri, router_ref2 )
    assert call_context2.caller == session2
    assert call_context2.caller_ref == caller_ref2



@pytest.mark.asyncio
async def test_get_call_same_user( dynamodb_table, helpers, alice, uri ):
    store = dynamodb.DynamoDBStore()
    session = helpers.create_session( dynamodb_table, alice )
    router_ref1 = helpers.uuid()
    caller_ref1 = helpers.uuid()
    router_ref2 = helpers.uuid()
    caller_ref2 = helpers.uuid()

    await store.put_call( session, uri, router_ref1, caller_ref1 )
    await store.put_call( session, uri, router_ref2, caller_ref2 )

    call_context1 = await store.get_call( session.realm, uri, router_ref1 )
    assert call_context1.caller == session
    assert call_context1.caller_ref == caller_ref1
    call_context2 = await store.get_call( session.realm, uri, router_ref2 )
    assert call_context2.caller == session
    assert call_context2.caller_ref == caller_ref2



@pytest.mark.asyncio
async def test_get_call_cross_realm( dynamodb_table, helpers, realm1, realm2, uri ):
    store = dynamodb.DynamoDBStore()
    user1 = helpers.create_user( dynamodb_table, realm1, 'bob' )
    session1 = helpers.create_session( dynamodb_table, user1 )
    router_ref1 = helpers.uuid()
    caller_ref1 = helpers.uuid()
    user2 = helpers.create_user( dynamodb_table, realm2, 'bob' )
    session2 = helpers.create_session( dynamodb_table, user2 )
    router_ref2 = helpers.uuid()
    caller_ref2 = helpers.uuid()

    await store.put_call( session1, uri, router_ref1, caller_ref1 )
    await store.put_call( session2, uri, router_ref2, caller_ref2 )

    call_context1 = await store.get_call( session1.realm, uri, router_ref1 )
    assert call_context1.caller == session1
    assert call_context1.caller_ref == caller_ref1
    call_context2 = await store.get_call( session2.realm, uri, router_ref2 )
    assert call_context2.caller == session2
    assert call_context2.caller_ref == caller_ref2



@pytest.mark.asyncio
async def test_delete_call( helpers, dynamodb_table, alice, uri ):
    store = dynamodb.DynamoDBStore()
    session = helpers.create_session( dynamodb_table, alice )
    router_ref = helpers.uuid()
    caller_ref = helpers.uuid()

    await store.put_call( session, uri, router_ref, caller_ref )
    await store.delete_call( session, uri, router_ref )

    with pytest.raises( exceptions.MissingInvocation ):
        await store.get_call( session.realm, uri, router_ref )

