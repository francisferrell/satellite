# mypy: allow-untyped-defs

import pytest

from satellite import exceptions
from satellite_lambda import dynamodb



@pytest.mark.asyncio
async def test_put_subscription( helpers, dynamodb_table, alice, uri ):
    store = dynamodb.DynamoDBStore()
    session = helpers.create_session( dynamodb_table, alice )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': f'Subscription#{uri}',
        }
    )
    assert 'Item' not in response
    assert await store.get_subscribers( session.realm, uri ) == []

    await store.put_subscription( session, uri )

    response = dynamodb_table.get_item(
        Key = {
            'pk': session.pk,
            'sk': f'Subscription#{uri}',
        }
    )
    assert 'Item' in response
    assert await store.get_subscribers( session.realm, uri ) == [ session ]



@pytest.mark.asyncio
async def test_get_subscribers_empty( helpers, realm, uri ):
    store = dynamodb.DynamoDBStore()
    assert await store.get_subscribers( realm, uri ) == []



@pytest.mark.asyncio
async def test_get_subscribers_multiple( dynamodb_table, helpers, realm, uri1, uri2 ):
    store = dynamodb.DynamoDBStore()
    user1 = helpers.create_user( dynamodb_table, realm, 'user1' )
    session1 = helpers.create_session( dynamodb_table, user1 )
    user2 = helpers.create_user( dynamodb_table, realm, 'user2' )
    session2 = helpers.create_session( dynamodb_table, user2 )

    await store.put_subscription( session1, uri1 )
    await store.put_subscription( session1, uri2 )
    await store.put_subscription( session2, uri2 )

    assert set( [ session1 ] ) == set( await store.get_subscribers( realm, uri1 ) )
    assert set( [ session1, session2 ] ) == set( await store.get_subscribers( realm, uri2 ) )



@pytest.mark.asyncio
async def test_get_subscribers_cross_realm( dynamodb_table, helpers, realm1, realm2, uri ):
    store = dynamodb.DynamoDBStore()
    user1 = helpers.create_user( dynamodb_table, realm1, 'bob' )
    session1 = helpers.create_session( dynamodb_table, user1 )
    user2 = helpers.create_user( dynamodb_table, realm2, 'bob' )
    session2 = helpers.create_session( dynamodb_table, user2 )

    await store.put_subscription( session1, uri )
    await store.put_subscription( session2, uri )

    assert set( [ session1 ] ) == set( await store.get_subscribers( realm1, uri ) )
    assert set( [ session2 ] ) == set( await store.get_subscribers( realm2, uri ) )

