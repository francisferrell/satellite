# mypy: allow-untyped-defs

from copy import deepcopy
import json
from pathlib import Path
from typing import Any

import argon2
import boto3
from moto import mock_aws
import pytest
from toolkit import conf
from uuid import uuid4

from satellite_lambda import dynamodb

CONF = conf.get_default_reader()
CONF.parse_env()

PasswordHasher = argon2.PasswordHasher(
    hash_len = 16,
    time_cost = 1,
)

fixtures_dir = Path( __file__ ).parent / 'fixtures'

def make_fixture( value: Any ) -> Any:
    @pytest.fixture
    def fixture():
        return deepcopy( value )
    return fixture

for fixture_path in fixtures_dir.glob( '*.json' ):
    fixture_name = fixture_path.name.replace( '.', '_' )
    with fixture_path.open( 'r' ) as fixture_file:
        globals()[fixture_name] = make_fixture( json.load( fixture_file ) )



@pytest.fixture( autouse = True )
def dynamodb_table():
    with mock_aws():
        aws_dynamodb_client = boto3.client( 'dynamodb' )
        aws_dynamodb_client.create_table(
            TableName = CONF.get( 'db.table_name' ),
            AttributeDefinitions = [
                {
                    'AttributeName': 'pk',
                    'AttributeType': 'S',
                },
                {
                    'AttributeName': 'sk',
                    'AttributeType': 'S',
                },
            ],
            KeySchema = [
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH',
                },
                {
                    'AttributeName': 'sk',
                    'KeyType': 'RANGE',
                },
            ],
            GlobalSecondaryIndexes = [
                {
                    'IndexName': 'SID',
                    'KeySchema': [
                        {
                            'AttributeName': 'sk',
                            'KeyType': 'HASH',
                        },
                        {
                            'AttributeName': 'pk',
                            'KeyType': 'RANGE',
                        },
                    ],
                    'Projection': {
                        'ProjectionType': 'ALL',
                    },
                },
            ],
            BillingMode = 'PAY_PER_REQUEST',
        )

        dynamodb.get_dynamodb_table.cache_clear()
        yield dynamodb.get_dynamodb_table()



@pytest.fixture( autouse = True )
def mock_apigw_session_send( mocker ):
    mocker.patch( 'satellite_lambda.dynamodb.APIGatewayAppSession.send' )



@pytest.fixture
def realm():
    return 'realm.local'

@pytest.fixture
def realm1():
    return 'realm1.local'

@pytest.fixture
def realm2():
    return 'realm2.local'



@pytest.fixture
def uri():
    return 'foo'

@pytest.fixture
def uri1():
    return 'uri.one'

@pytest.fixture
def uri2():
    return 'uri.two'



class Helpers:

    @staticmethod
    def uuid():
        return str( uuid4() )

    @staticmethod
    def create_user( dynamodb_table, realm, name ):
        user = dynamodb.DynamoDBAppUser( realm = realm, name = name, role = 'anonymous' )
        dynamodb_table.put_item(
            Item = {
                'pk': user.pk,
                'sk': 'profile',
                'role': user.role,
                'password_hash': PasswordHasher.hash( user.name ),
            },
        )
        return user

    @staticmethod
    def create_session( dynamodb_table, user ):
        session = dynamodb.APIGatewayAppSession(
            realm = user.realm,
            session_id = Helpers.uuid(), # type: ignore[no-untyped-call]
            role = 'anonymous',
        )
        dynamodb_table.put_item(
            Item = {
                'pk': session.pk,
                'sk': '_',
                'role': session.role,
            },
        )
        return session

@pytest.fixture
def helpers():
    return Helpers



@pytest.fixture
def alice( helpers, dynamodb_table, realm1 ):
    return helpers.create_user( dynamodb_table, realm1, 'alice' )

