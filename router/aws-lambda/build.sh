#!/bin/bash

set -euxo pipefail



if [[ $1 == devenv ]] ; then
    rm -rf venv
    python3 -m venv venv

    PIP_INDEX_URL="$TOOLKIT_PIP_INDEX_URL" ./venv/bin/pip install -c requirements.txt toolkit
    ./venv/bin/pip install -e ../../sdk/python/
    ./venv/bin/pip install -e '.[dev]'
elif [[ $1 == venv ]] ; then
    rm -rf venv
    python3 -m venv venv

    PIP_INDEX_URL="$TOOLKIT_PIP_INDEX_URL" ./venv/bin/pip install -c requirements.txt toolkit
    ./venv/bin/pip install -c requirements.txt ../../sdk/python/
    ./venv/bin/pip install -c requirements.txt '.[dev]'
elif [[ $1 == devlambda ]] ; then
    tmp=$( mktemp --directory --tmpdir=. tmp.build.XXXXXX )

    pip install \
        --platform manylinux2014_aarch64 \
        --implementation cp \
        --target="$tmp" \
        --only-binary=:all: \
        --constraint requirements.txt \
        ../../sdk/python

    PIP_INDEX_URL="$TOOLKIT_PIP_INDEX_URL" pip install \
        --platform manylinux2014_aarch64 \
        --implementation cp \
        --target="$tmp" \
        --only-binary=:all: \
        --constraint requirements.txt \
        .

    ( cd "$tmp"; find . -type d -name __pycache__ -prune -print -exec rm -rf {} \; )
    rm -f package.zip
    python3 -c 'import shutil, sys; shutil.make_archive(sys.argv[1], "zip", sys.argv[2])' package "$tmp"
    rm -rf "$tmp"
elif [[ $1 == lambda ]] ; then
    tmp=$( mktemp --directory --tmpdir=. tmp.build.XXXXXX )

    PIP_INDEX_URL="$SATELLITE_PIP_INDEX_URL" pip install \
        --platform manylinux2014_aarch64 \
        --implementation cp \
        --target="$tmp" \
        --only-binary=:all: \
        --constraint requirements.txt \
        "satellite${SATELLITE_PIP_CONSTRAINT}"

    PIP_INDEX_URL="$TOOLKIT_PIP_INDEX_URL" pip install \
        --platform manylinux2014_aarch64 \
        --implementation cp \
        --target="$tmp" \
        --only-binary=:all: \
        --constraint requirements.txt \
        .

    ( cd "$tmp"; find . -type d -name __pycache__ -prune -print -exec rm -rf {} \; )
    rm -f package.zip
    python3 -c 'import shutil, sys; shutil.make_archive(sys.argv[1], "zip", sys.argv[2])' package "$tmp"
    rm -rf "$tmp"
fi

