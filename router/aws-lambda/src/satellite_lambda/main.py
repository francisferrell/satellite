"""
Satellite Lambda function.
"""

import asyncio
from functools import cache
from http import HTTPStatus
import json
from typing import Any, TypedDict

from satellite import common, exceptions, router
from toolkit import conf
from toolkit.logging import getLogger

from satellite_lambda import dynamodb

logger = getLogger( __name__ )
CONF = conf.get_default_reader()
CONF.parse_env()



@cache
def get_dynamodb_store() -> dynamodb.DynamoDBStore:
    """
    Get the singular dynamodb.DynamoDBStore instance
    """
    return dynamodb.DynamoDBStore()

@cache
def get_router() -> router.Router:
    """
    Get the singular Router instance
    """
    return router.Router( get_dynamodb_store() )



class Response( TypedDict ):
    """
    Response dictionary type structure.
    """
    statusCode: int
    body: str



def response( status: HTTPStatus, body: str|None = None ) -> Response:
    """
    Return a valid API Gateway response object from `status`
    """
    return {
        'statusCode': status.value,
        'body': body or status.phrase,
    }



async def handle_connect( session: dynamodb.APIGatewayAppSession, source_ip: str ) -> Response:
    """
    Handle API gateway websocket connect events.
    """
    logger.warning(
        'new session: realm=%r session_id=%r source_ip=%r',
        session.realm,
        session.session_id,
        source_ip,
    )
    await get_dynamodb_store().put_session( session )
    return response( HTTPStatus.OK )



async def handle_disconnect( session: dynamodb.APIGatewayAppSession, source_ip: str ) -> Response:
    """
    Handle API gateway websocket disconnect events.
    """
    logger.warning(
        'end session: realm=%r session_id=%r source_ip=%r',
        session.realm,
        session.session_id,
        source_ip,
    )
    await get_dynamodb_store().delete_session( session )
    return response( HTTPStatus.OK )



async def handle_message( session: dynamodb.APIGatewayAppSession, body: str ) -> Response:
    """
    Handle `eventType == 'MESSAGE'`.
    """
    logger.debug( 'message: session=%r body=%r', session, body )
    try:
        message = common.Message.fromjson( body )
    except exceptions.InvalidMessage:
        logger.warning( 'malformed message: session=%r body=%r', session, body )
        return response( HTTPStatus.BAD_REQUEST )

    logger.info(
        'routing message: realm=%r session_id=%r message=%r',
        session.realm,
        session.session_id,
        message
    )
    result = await get_router().route( message, session )
    return response( HTTPStatus.OK, result.tojson() )



async def handle_apigw_ws_event( event: Any ) -> Response:
    """
    Handle events from an API Gateway websocket API.
    """
    context = event['requestContext']
    source_ip = context.get( 'identity', {} ).get( 'sourceIp', 'unknown' )
    domain = context['domainName']
    # The realm will be this API Gateway's Management API's endpoint URL, which is either the AWS-provided
    # domain/stage or is the custom domain (which implies stage). See boto3 docs for more details.
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/apigatewaymanagementapi.html#ApiGatewayManagementApi.Client
    if domain.endswith( '.amazonaws.com' ):
        realm = f'{domain}/{context["stage"]}'
    else:
        realm = domain

    session = dynamodb.APIGatewayAppSession(
        realm = realm,
        session_id = context['connectionId'],
        role = 'anonymous',
    )

    match context['eventType']:
        case 'CONNECT':
            return await handle_connect( session, source_ip )
        case 'DISCONNECT':
            return await handle_disconnect( session, source_ip )
        case 'MESSAGE':
            return await handle_message( session, event['body'] )
        case _:
            logger.warning( f'unexpected apigw websocket requestContext: {json.dumps(context)}' )
            return response( HTTPStatus.BAD_REQUEST )



def handle_meta_event( satellite_meta: Any ) -> Response:
    """
    Handle `satelliteMeta` events.
    """
    match satellite_meta:
        case 'warm':
            logger.warning( 'toasty lambda' )
            return response( HTTPStatus.OK )
        case _:
            logger.warning( f'unexpected meta: {json.dumps(satellite_meta)}' )
            return response( HTTPStatus.BAD_REQUEST )



def handler( event: Any, _: Any ) -> Response:
    """
    AWS Lambda entrypoint
    """
    try:
        #logger.debug( json.dumps( event, sort_keys = True ) )
        match event:
            case { 'satelliteMeta': satellite_meta }:
                return handle_meta_event( satellite_meta )
            case { 'requestContext': _ }:
                loop = asyncio.get_event_loop()
                return loop.run_until_complete( handle_apigw_ws_event( event ) )
            case _:
                logger.warning( f'unexpected event: {json.dumps(event)}' )
                return response( HTTPStatus.BAD_REQUEST )
    except: # pylint: disable=bare-except
        logger.exception( 'unhandled exception' )
        return response( HTTPStatus.INTERNAL_SERVER_ERROR )

