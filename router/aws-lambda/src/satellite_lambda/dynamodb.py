"""
Satellite router store implemented on DynamoDB using single table design.
"""

from functools import cache
from typing import TYPE_CHECKING

import boto3
from satellite import exceptions
from satellite.router.store import AppSession, AppUser, CallContext, Store
from toolkit import conf
from toolkit.logging import getLogger

CONF = conf.get_default_reader()

if TYPE_CHECKING:
    from mypy_boto3_apigatewaymanagementapi import ApiGatewayManagementApiClient
    from mypy_boto3_dynamodb.client import DynamoDBClient
    from mypy_boto3_dynamodb.service_resource import Table

logger = getLogger( __name__ )



@cache
def get_websocket_management_api( api_domain: str ) -> 'ApiGatewayManagementApiClient':
    """
    Returns boto3 apigatewaymanagementapi client for API gateway endpoint `api_domain`.

    Per the boto3 docs, `endpoint_url` must be either:
    - of the form `https://{api-id}.execute-api.{region}.amazonaws.com/{stage}`
    - the API’s custom domain and base path, if applicable.

    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/apigatewaymanagementapi.html
    """
    return boto3.client(
        'apigatewaymanagementapi',
        # TODO: formalize the valid values and where they come from after experimenting with APIGWs
        # that both have and lack custom domain names.
        endpoint_url = f'https://{api_domain}',
    )



def get_table_name() -> str:
    """
    The configured Dynamodb Table name
    """
    return CONF.get( 'db.table_name' )



@cache
def get_dynamodb_table() -> 'Table':
    """
    The Dynamodb Table
    """
    return boto3.resource( 'dynamodb' ).Table( get_table_name() )



@cache
def get_dynamodb_client() -> 'DynamoDBClient':
    """
    The Dynamodb Client
    """
    return boto3.client( 'dynamodb' )



class APIGatewayAppSession( AppSession ):
    """
    An application session connected to an AWS API Gateway v2 websocket API.

    The `realm` must be the API gateway `domainName` (and possibly `/stage`). The `session_id` must
    be the `connectionId`.
    """

    @staticmethod
    def make_pk( realm: str, session_id: str ) -> str:
        """
        The value for the `pk` field of the given session.
        """
        return f'Session#{realm}#{session_id}'

    @property
    def pk( self ) -> str:
        """
        The value for the `pk` field of this session in DynamoDB.
        """
        return APIGatewayAppSession.make_pk( self.realm, self.session_id )



    async def send( self, data: str ) -> None:
        """
        Send `data` to the application session, awaiting the completion of that write operation.
        """
        get_websocket_management_api( self.realm ).post_to_connection(
            ConnectionId = self.session_id,
            Data = data.encode(),
        )



class DynamoDBAppUser( AppUser ): # pylint: disable=too-few-public-methods
    """
    AppUser class with conveniences for DynamoDBStore.
    """

    @staticmethod
    def make_pk( realm: str, name: str ) -> str:
        """
        The value for the `pk` field of the given user.
        """
        return f'User#{realm}#{name}'

    @property
    def pk( self ) -> str:
        """
        The value for the `pk` field of this user in DynamoDB.
        """
        return DynamoDBAppUser.make_pk( self.realm, self.name )



class DynamoDBStore( Store ):
    """
    Satellite store implemented in AWS DynamoDB using single table design.
    """

    async def get_user( self, realm: str, username: str ) -> DynamoDBAppUser:
        """
        Get the User on `realm`.

        Should raise `satellite.exceptions.MissingUser` if the user is not found in the specified realm.
        """
        response = get_dynamodb_table().get_item(
            Key = {
                'pk': DynamoDBAppUser.make_pk( realm, username ),
                'sk': 'profile',
            }
        )

        if 'Item' not in response:
            raise exceptions.MissingUser()

        return DynamoDBAppUser(
            realm = realm,
            name = username,
            role = str( response['Item']['role'] ),
        )



    async def put_session(
        self,
        session: APIGatewayAppSession, # type: ignore[override]
    ) -> None:
        """
        Store the `session`.
        """
        logger.debug( 'put_session: session=%r', session )
        get_dynamodb_table().put_item(
            Item = {
                'pk': session.pk,
                'sk': '_',
            },
        )



    async def delete_session(
        self,
        session: APIGatewayAppSession, # type: ignore[override]
    ) -> None:
        """
        Delete the provided `session` and all data associated with it.
        """
        logger.debug( 'delete_session: session=%r', session )
        response = get_dynamodb_client().get_paginator( 'query' ).paginate(
            TableName = get_table_name(),
            KeyConditionExpression = 'pk = :pk',
            ExpressionAttributeValues = {
                ":pk": { "S": session.pk },
            },
        )

        with get_dynamodb_table().batch_writer() as batch:
            for page in response:
                for item in page.get( 'Items', [] ):
                    batch.delete_item(
                        Key = {
                            'pk': item['pk']['S'],
                            'sk': item['sk']['S'],
                        }
                    )



    async def put_subscription(
        self,
        subscriber: APIGatewayAppSession, # type: ignore[override]
        uri: str
    ) -> None:
        """
        Store the subscription by `subscriber` to the topic at `uri` on the subscriber's realm.
        """
        logger.debug( 'put_subscription: subscriber=%r uri=%r', subscriber, uri )
        get_dynamodb_table().put_item(
            Item = {
                'pk': subscriber.pk,
                # TODO should all of this style of sk value encode the realm?
                'sk': f'Subscription#{uri}',
            },
        )



    async def get_subscribers( # type: ignore[override]
        self,
        realm: str,
        uri: str,
    ) -> list[APIGatewayAppSession]:
        """
        Get the current subscribers of the topic at `uri` on `realm`.
        """
        paginator = get_dynamodb_client().get_paginator( 'query' )
        pk_prefix = f'Session#{realm}#'
        response = paginator.paginate(
            TableName = get_table_name(),
            IndexName = 'SID',
            KeyConditionExpression = 'sk = :sk AND begins_with( pk, :pk )',
            ExpressionAttributeValues = {
                ":sk": { "S": f'Subscription#{uri}' },
                ":pk": { "S": pk_prefix },
            },
        )

        pk_prefix_len = len( pk_prefix )
        subscribers = []
        for page in response:
            for item in page.get( 'Items', [] ):
                session_id = item['pk']['S'][pk_prefix_len:]
                subscribers.append( APIGatewayAppSession(
                    realm = realm,
                    session_id = session_id,
                    # TODO actually get the session details correctly
                    role = 'anonymous',
                ) )

        return subscribers



    async def put_registration(
        self,
        registrant: APIGatewayAppSession, # type: ignore[override]
        uri: str
    ) -> None:
        """
        Store the registration of `registrant` for the RPC at `uri` on the registrant's realm.
        """
        logger.debug( 'put_registration: registrant=%r uri=%r', registrant, uri )
        # TODO push the uniqueness constraint on registrations down to this level
        get_dynamodb_table().put_item(
            Item = {
                'pk': registrant.pk,
                'sk': f'Registration#{uri}',
            },
        )



    async def get_registrant( self, realm: str, uri: str ) -> AppSession:
        """
        Get the current registrant of the RPC at `uri` on `realm`.
        """
        pk_prefix = f'Session#{realm}#'
        response = get_dynamodb_client().query(
            TableName = get_table_name(),
            IndexName = 'SID',
            KeyConditionExpression = 'sk = :sk AND begins_with( pk, :pk )',
            ExpressionAttributeValues = {
                ":sk": { "S": f'Registration#{uri}' },
                ":pk": { "S": pk_prefix },
            },
            Limit = 1,
        )

        if len( response['Items'] ) == 0:
            raise exceptions.MissingRegistration( f'RPC is not registered: {uri=}' )

        pk_prefix_len = len( pk_prefix )
        item = response['Items'][0]
        session_id = item['pk']['S'][pk_prefix_len:]
        return APIGatewayAppSession(
            realm = realm,
            session_id = session_id,
            # TODO actually get the session details correctly
            role = 'anonymous',
        )



    async def put_call(
        self,
        caller: APIGatewayAppSession, # type: ignore[override]
        uri: str,
        router_ref: str,
        caller_ref: str
    ) -> None:
        """
        Store the call by `caller` to the RPC at `uri` on the caller's realm, with the provided refs.
        """
        logger.debug(
            'put_call: caller=%r uri=%r router_ref=%r caller_ref=%r',
            caller,
            uri,
            router_ref,
            caller_ref,
        )
        get_dynamodb_table().put_item(
            Item = {
                'pk': caller.pk,
                'sk': f'Call#{uri}#{router_ref}',
                'caller_ref': caller_ref,
            },
        )



    async def delete_call(
        self,
        caller: APIGatewayAppSession, # type: ignore[override]
        uri: str,
        router_ref: str
    ) -> None:
        """
        Delete the call by `caller` to the RPC at `uri` on the caller's realm tracked by the provided
        `router_ref`.
        """
        logger.debug(
            'delete_call: caller=%r uri=%r router_ref=%r',
            caller,
            uri,
            router_ref,
        )
        get_dynamodb_table().delete_item(
            Key = {
                'pk': caller.pk,
                'sk': f'Call#{uri}#{router_ref}',
            },
        )



    async def get_call( self, realm: str, uri: str, router_ref: str ) -> CallContext:
        """
        Get the context of the call to the RPC at `uri` on `realm` tracked by the provided `router_ref`.
        """
        pk_prefix = f'Session#{realm}#'

        response = get_dynamodb_client().query(
            TableName = get_table_name(),
            IndexName = 'SID',
            KeyConditionExpression = 'sk = :sk AND begins_with( pk, :pk )',
            ExpressionAttributeValues = {
                ":sk": { "S": f'Call#{uri}#{router_ref}' },
                ":pk": { "S": pk_prefix },
            },
        )

        if len( response['Items'] ) == 0:
            raise exceptions.MissingInvocation( f'RPC has not been called: {uri=}' )

        item = response['Items'][0]
        pk_prefix_len = len( pk_prefix )
        caller_session_id = item['pk']['S'][pk_prefix_len:]
        caller_ref = item['caller_ref']['S']

        return CallContext(
            caller = APIGatewayAppSession(
                realm = realm,
                session_id = caller_session_id,
                # TODO actually get the session details correctly
                role = 'anonymous',
            ),
            caller_ref = caller_ref,
        )

