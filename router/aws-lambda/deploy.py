#!/usr/bin/env python3

from argparse import ArgumentParser, FileType
from os import environ
from sys import exit

import requests

PROJECT_URL = f'{ environ["CI_API_V4_URL"] }/projects/{ environ["CI_PROJECT_ID"] }'



def main():
    parser = ArgumentParser(
        prog = 'deploy.py',
        description = 'deploy the AWS Lambda package to the project generic package repository',
    )
    parser.add_argument(
        '--package-name', '-p',
        required = True,
    )
    parser.add_argument(
        '--version', '-v',
        required = True,
    )
    parser.add_argument(
        '--file-name', '-f',
        required = True,
    )
    parser.add_argument(
        'package_zip',
        type = FileType( 'rb' ),
    )
    args = parser.parse_args()
    print( repr( args ) )

    with requests.Session() as https:
        if 'CI_JOB_TOKEN' in environ:
            https.headers.update( { 'JOB-TOKEN': environ['CI_JOB_TOKEN'] } )
        elif 'GITLAB_TOKEN' in environ:
            https.headers.update( { 'PRIVATE-TOKEN': environ['GITLAB_TOKEN'] } )

        print( f'deploying generic package {args.package_name} @ {args.version} with file {args.file_name}' )
        https.put(
            f'{PROJECT_URL}/packages/generic/{args.package_name}/{args.version}/{args.file_name}',
            data = args.package_zip
        ).raise_for_status()



if __name__ == '__main__':
    exit( main() )

