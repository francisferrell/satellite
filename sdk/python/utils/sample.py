#!./venv/bin/python

import argparse
import asyncio
from pathlib import Path
from sys import exit

import satellite



async def handle_echo( reader, writer ):
    data = await reader.read( 100 )
    message = data.decode()
    addr = writer.get_extra_info( 'peername' )

    print( f"Echoing {message!r} to {addr!r}" )
    writer.write( data )
    await writer.drain()
    writer.close()
    await writer.wait_closed()



async def start_echo_server():
    echo_server = await asyncio.start_server( handle_echo, '127.0.0.1', 8888 )
    addrs = ', '.join( ':'.join( map( str, sock.getsockname() ) ) for sock in echo_server.sockets )
    print( f'Serving echos on {addrs}' )
    await echo_server.serve_forever()



def on_foo_bar( kwargs ):
    print( f'foo.bar: {kwargs}' )



async def start_satellite( router, username, password ):
    satellite_client = satellite.SatelliteClient()
    await satellite_client.connect( router, username, password )
    await satellite_client.register( 'foo.bar', on_foo_bar )
    await satellite_client.run_forever()



async def main():
    parser = argparse.ArgumentParser(
        prog = 'sample',
        description = 'sample satellite application',
    )
    parser.add_argument(
        '-c', '--credentials',
        help = 'credentials to connect with',
        type = Path,
        required = True,
    )
    parser.add_argument(
        'router',
        help = 'websocket URI of the satellite router',
    )
    args = parser.parse_args()

    if args.credentials.stat().st_mode & 0o077 > 0:
        print( f'refusing to read credentials from overly permissive file: {args.credentials}' )
        exit( 1 )
    with args.credentials.open( 'r' ) as f:
        credentials = f.read().rstrip( '\n' ).split( ':', maxsplit = 1 )

    satellite_task = asyncio.create_task( start_satellite( args.router, credentials[0], credentials[1] ) )
    echo_server_task = asyncio.create_task( start_echo_server() )

    try:
        tasks = asyncio.gather( satellite_task, echo_server_task )
        await tasks
    except satellite.exceptions.InvalidCredentials as err:
        print( err )
    except asyncio.CancelledError:
        print( 'cancelled' )
        tasks.cancel()

asyncio.run( main() )

