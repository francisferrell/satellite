#!./venv/bin/python

import argparse
import asyncio
from getpass import getuser, getpass
import json
from pathlib import Path
from sys import exit

from toolkit import logging
logging.basicConfig()

from satellite import Client



def topic_logger( kwargs ):
    print( f'event: {kwargs}' )



async def main():
    parser = argparse.ArgumentParser(
        prog = 'client',
        description = 'interact with satellite from the CLI',
    )
    parser.add_argument(
        '-u', '--username',
        help = 'User to connect as',
    )
    parser.add_argument(
        '-c', '--credentials',
        help = 'credentials to connect with',
        type = Path,
    )
    parser.add_argument(
        'action',
        choices = [ 'subscribe', 'publish', 'register', 'call' ],
        help = 'Action to be performed',
    )
    parser.add_argument(
        'uri',
        help = 'URI to perform action on',
    )
    parser.add_argument(
        'kwargs',
        help = 'json object to pass with publish',
        nargs = '?',
        default = '{}',
    )
    args = parser.parse_args()

    if args.credentials is not None:
        if args.credentials.stat().st_mode & 0o077 > 0:
            print( f'refusing to read credentials from overly permissive file: {args.credentials}' )
            exit( 1 )
        with args.credentials.open( 'r' ) as f:
            credentials = f.read().rstrip( '\n' )
            args.username, password = credentials.split( ':', maxsplit = 1 )
    else:
        if args.username is None:
            args.username = getuser()
        password = getpass()
        auth = basic_auth( args.username, password )

    client = Client()
    await client.connect( 'wss://satellite.francisferrell.dev', args.username, password )
    print( 'connected' )

    if args.action == 'subscribe':
        await client.subscribe( args.uri, topic_logger )
        print( 'completed subscribe' )
    if args.action == 'publish':
        await client.publish( args.uri, json.loads( args.kwargs ) )
        print( 'completed publish' )

    try:
        await client.run_forever()
    except asyncio.CancelledError:
        pass

if __name__ == '__main__':
    asyncio.run( main() )

