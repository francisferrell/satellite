# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_publish_request( publish_request_raw ):
    message = common.Message.fromjson( json.dumps( publish_request_raw ) )
    assert isinstance( message, common.PublishRequest )
    assert message.uri == publish_request_raw['uri']
    assert message.ref == publish_request_raw['ref']
    assert message.kwargs == publish_request_raw['kwargs']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'kwargs' ] )
def test_publish_request_required_fields( publish_request_raw, required_field ):
    publish_request_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( publish_request_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

