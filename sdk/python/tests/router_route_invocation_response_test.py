# mypy: allow-untyped-defs

import asyncio
from unittest.mock import AsyncMock

import pytest

from satellite import common
from satellite.router.store import CallContext



@pytest.mark.asyncio
async def test_route_invocation_response( app_session, router, uri, result, helpers ):
    router_ref = common.make_ref()
    caller = helpers.new_app_session()
    caller_ref = common.make_ref()
    context = CallContext(
        caller = caller,
        caller_ref = caller_ref
    )
    router._store.get_call = AsyncMock( return_value = context )

    request = common.InvocationResponse(
        ref = router_ref,
        uri = uri,
        result = result,
    )

    route_task = asyncio.create_task( router.route( request, app_session ) )

    call_response = await caller.sent_message()
    assert isinstance( call_response, common.CallResponse )
    assert call_response.uri == request.uri
    assert call_response.ref == caller_ref
    assert call_response.result == request.result

    args = await router._store.delete_call_args()
    assert args[0] is app_session
    assert args[1] == request.uri
    assert args[2] == router_ref

    confirmation = await route_task
    assert isinstance( confirmation, common.Confirmation )
    assert confirmation.request_type == common.ConfirmableMessageType.InvocationResponse
    assert confirmation.uri == request.uri
    assert confirmation.ref == router_ref



@pytest.mark.asyncio
async def test_route_invocation_response_missing( helpers, app_session, router, uri, result ):
    request = common.InvocationResponse(
        uri = uri,
        result = result,
    )

    rejection = await router.route( request, app_session )
    assert isinstance( rejection, common.Rejection )
    assert rejection.request_type == common.ConfirmableMessageType.InvocationResponse
    assert rejection.uri == request.uri
    assert rejection.ref == request.ref
    assert rejection.klass == 'MissingInvocation'

    router._store.delete_call_mock.assert_not_awaited()

