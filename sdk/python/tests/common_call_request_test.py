# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_call_request( call_request_raw ):
    message = common.Message.fromjson( json.dumps( call_request_raw ) )
    assert isinstance( message, common.CallRequest )
    assert message.uri == call_request_raw['uri']
    assert message.ref == call_request_raw['ref']
    assert message.kwargs == call_request_raw['kwargs']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'kwargs' ] )
def test_call_request_required_fields( call_request_raw, required_field ):
    call_request_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( call_request_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

