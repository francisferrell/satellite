# mypy: allow-untyped-defs

import asyncio

import pytest

from satellite import app
from satellite.app.component import Component
from satellite.app.transport import Transport, TransportAdapter



class MyTransport( Transport ):

    async def send( self, *args, **kwargs ):
        pass

    async def recv( self, *args, **kwargs ):
        await asyncio.Event().wait()
        return ''

    async def close( self, *args, **kwargs ):
        pass



class MyTransportAdapter( TransportAdapter ):

    async def __anext__( self ):
        return MyTransport()



class MyComponent( Component ):

    def __init__( self ) -> None:
        super().__init__()
        self.after_connected_event = asyncio.Event()
        self.after_disconnected_event = asyncio.Event()

    async def after_connected( self, session ):
        self.after_connected_event.set()

    async def after_disconnected( self ):
        self.after_disconnected_event.set()



@pytest.mark.asyncio
async def test_run():
    transport_adapter = MyTransportAdapter()
    component1 = MyComponent()
    component2 = MyComponent()

    run_task = asyncio.create_task(
        app.run( transport_adapter, [ component1, component2 ] )
    )

    async with asyncio.timeout( 2 ):
        await component1.after_connected_event.wait()
        await component2.after_connected_event.wait()

    assert not component1.after_disconnected_event.is_set()
    assert not component2.after_disconnected_event.is_set()

    run_task.cancel()

    async with asyncio.timeout( 2 ):
        await component1.after_disconnected_event.wait()
        await component2.after_disconnected_event.wait()

    await run_task



@pytest.mark.asyncio
async def test_run_failed_connection():
    class TheServerDoesntLikeYou( Exception ):
        pass

    class BadTransportAdapter( TransportAdapter ):
        async def __anext__( self ):
            raise TheServerDoesntLikeYou

    transport_adapter = BadTransportAdapter()
    component = MyComponent()

    run_task = asyncio.create_task(
        app.run( transport_adapter, [ component ] )
    )

    async with asyncio.timeout( 2 ):
        try:
            await run_task
        except TheServerDoesntLikeYou:
            pass

    assert not component.after_connected_event.is_set()
    assert not component.after_disconnected_event.is_set()

