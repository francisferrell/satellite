# mypy: allow-untyped-defs

import pytest

from satellite import common
from satellite.app import session



@pytest.mark.asyncio
async def test_call_response( connected_session, uri, result ):
    call = session.Call(
        request_type = common.ConfirmableMessageType.CallRequest,
        uri = uri,
    )
    connected_session._calls[call.ref] = call

    call_response = common.CallResponse(
        ref = call.ref,
        uri = call.uri,
        result = result,
    )
    await connected_session.handle_message( call_response )

    assert call.result.result() == result



@pytest.mark.asyncio
async def test_call_response_unexpected( connected_session, uri, result ):
    call_response = common.CallResponse(
        uri = uri,
        result = result,
    )
    assert call_response.uri not in connected_session._calls
    await connected_session.handle_message( call_response )

