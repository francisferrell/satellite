# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_topic_event( topic_event_raw ):
    message = common.Message.fromjson( json.dumps( topic_event_raw ) )
    assert isinstance( message, common.TopicEvent )
    assert message.uri == topic_event_raw['uri']
    assert message.ref == topic_event_raw['ref']
    assert message.kwargs == topic_event_raw['kwargs']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'kwargs' ] )
def test_topic_event_required_fields( topic_event_raw, required_field ):
    topic_event_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( topic_event_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

