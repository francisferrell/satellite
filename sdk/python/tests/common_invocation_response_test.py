# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_invocation_response( invocation_response_raw ):
    message = common.Message.fromjson( json.dumps( invocation_response_raw ) )
    assert isinstance( message, common.InvocationResponse )
    assert message.uri == invocation_response_raw['uri']
    assert message.ref == invocation_response_raw['ref']
    assert message.result == invocation_response_raw['result']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'result' ] )
def test_invocation_response_required_fields( invocation_response_raw, required_field ):
    invocation_response_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( invocation_response_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

