# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



CONFIRMABLE_TYPES = common.ConfirmableMessageType.__members__.keys()

@pytest.mark.parametrize( 'request_type', CONFIRMABLE_TYPES )
def test_rejection( rejection_raw, request_type ):
    rejection_raw.update( request_type = request_type )
    message = common.Message.fromjson( json.dumps( rejection_raw ) )
    assert isinstance( message, common.Rejection )
    assert message.uri == rejection_raw['uri']
    assert message.ref == rejection_raw['ref']
    assert message.request_type == rejection_raw['request_type']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'request_type', 'klass', 'args' ] )
def test_rejection_required_fields( rejection_raw, required_field ):
    rejection_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( rejection_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

