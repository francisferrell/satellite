# mypy: allow-untyped-defs

import asyncio
from unittest.mock import AsyncMock

import pytest

from satellite import common



@pytest.mark.asyncio
async def test_route_publish_request( app_session, router, uri, kwargs, helpers ):
    subscriber1 = helpers.new_app_session()
    subscriber2 = helpers.new_app_session()
    router._store.get_subscribers = AsyncMock( return_value = [ subscriber1, subscriber2 ] )

    request = common.PublishRequest(
        uri = uri,
        kwargs = kwargs,
    )

    route_task = asyncio.create_task( router.route( request, app_session ) )

    message1 = await subscriber1.sent_message()
    message2 = await subscriber2.sent_message()

    assert message1.uri == request.uri
    assert message1.ref == ''
    assert message1.kwargs == request.kwargs
    assert message1 == message2

    confirmation = await route_task
    assert confirmation.request_type == common.ConfirmableMessageType.PublishRequest
    assert confirmation.uri == request.uri
    assert confirmation.ref == request.ref



@pytest.mark.asyncio
async def test_route_publish_request_no_subscribers( app_session, router, uri, kwargs ):
    router._store.get_subscribers = AsyncMock( return_value = [] )

    request = common.PublishRequest(
        uri = uri,
        kwargs = kwargs,
    )

    confirmation = await router.route( request, app_session )
    assert confirmation.request_type == common.ConfirmableMessageType.PublishRequest
    assert confirmation.uri == request.uri
    assert confirmation.ref == request.ref



@pytest.mark.asyncio
async def test_route_publish_request_bad_subscriber( app_session, router, uri, kwargs, helpers ):
    subscriber1 = helpers.new_app_session()
    subscriber2 = helpers.new_app_session()
    subscriber1.mock_send.configure_mock( side_effect = RuntimeError )
    router._store.get_subscribers = AsyncMock( return_value = [ subscriber1, subscriber2 ] )

    request = common.PublishRequest(
        uri = uri,
        kwargs = kwargs,
    )

    route_task = asyncio.create_task( router.route( request, app_session ) )

    message2 = await subscriber2.sent_message()
    subscriber1.mock_send.assert_awaited_once()

    assert message2.uri == request.uri
    assert message2.ref == ''
    assert message2.kwargs == request.kwargs

    confirmation = await route_task
    assert confirmation.request_type == common.ConfirmableMessageType.PublishRequest
    assert confirmation.uri == request.uri
    assert confirmation.ref == request.ref

