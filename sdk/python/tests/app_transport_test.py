# mypy: allow-untyped-defs

from unittest.mock import AsyncMock

import pytest

from satellite.app.transport import Transport, TransportAdapter



class MyTransport( Transport ):

    def __init__( self ) -> None:
        super().__init__()
        self.close_mock = AsyncMock()

    async def send( self, *args, **kwargs ):
        pass

    async def recv( self, *args, **kwargs ):
        pass

    async def close( self, *args, **kwargs ):
        await self.close_mock( *args, **kwargs )



@pytest.mark.asyncio
async def test_close():
    t = MyTransport()

    async with t:
        pass

    t.close_mock.assert_called_once()



@pytest.mark.asyncio
async def test_close_exception():
    t = MyTransport()

    try:
        async with t:
            1 / 0
    except:
        pass

    t.close_mock.assert_called_once()



@pytest.mark.asyncio
async def test_transport_adapter():
    t = MyTransport()
    class MyTransportAdapter( TransportAdapter ):
        async def __anext__( self ):
            return t

    c = MyTransportAdapter()
    assert aiter( c ) is c
    assert await anext( c ) is t

