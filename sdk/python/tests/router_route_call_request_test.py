# mypy: allow-untyped-defs

import asyncio
from unittest.mock import AsyncMock

import pytest

from satellite import common



@pytest.mark.asyncio
async def test_route_call_request( app_session, router, uri, kwargs, helpers ):
    registrant = helpers.new_app_session()
    router._store.get_registrant = AsyncMock( return_value = registrant )

    request = common.CallRequest(
        uri = uri,
        kwargs = kwargs,
    )
    caller_ref = request.ref

    route_task = asyncio.create_task( router.route( request, app_session ) )

    args = await router._store.put_call_args()
    assert args[0] is app_session
    assert args[1] == request.uri
    router_ref = args[2]
    assert args[3] == caller_ref
    assert router_ref != caller_ref

    invocation_request = await registrant.sent_message()
    assert isinstance( invocation_request, common.InvocationRequest )
    assert invocation_request.uri == request.uri
    assert invocation_request.ref == router_ref
    assert invocation_request.kwargs == request.kwargs

    confirmation = await route_task
    assert isinstance( confirmation, common.Confirmation )
    assert confirmation.request_type == common.ConfirmableMessageType.CallRequest
    assert confirmation.uri == request.uri
    assert confirmation.ref == caller_ref



@pytest.mark.asyncio
async def test_route_call_request_missing( helpers, app_session, router, uri, kwargs ):
    request = common.CallRequest(
        uri = uri,
        kwargs = kwargs,
    )

    rejection = await router.route( request, app_session )
    assert isinstance( rejection, common.Rejection )
    assert rejection.request_type == common.ConfirmableMessageType.CallRequest
    assert rejection.uri == request.uri
    assert rejection.ref == request.ref
    assert rejection.klass == 'MissingRegistration'

    router._store.put_call_mock.assert_not_awaited()

