# mypy: allow-untyped-defs

import asyncio

import pytest

from satellite import common
from satellite.app import session



@pytest.mark.asyncio
async def test_publish( connected_session, uri, kwargs ):
    publish_task = asyncio.create_task( connected_session.publish( uri, kwargs ) )
    publish_request = await connected_session._transport.sent_message()

    assert isinstance( publish_request, common.PublishRequest )
    assert publish_request.uri == uri
    assert publish_request.kwargs == kwargs

    subject = connected_session._unconfirmed[publish_request.ref]
    assert subject.uri == uri
    assert subject.ref == publish_request.ref
    assert subject.state == session.ConfirmableState.REQUESTED
    assert not subject.confirmed.done()
    assert not publish_task.done()

    await connected_session.handle_message( common.Confirmation(
        uri = subject.uri,
        ref = subject.ref,
        request_type = common.ConfirmableMessageType.PublishRequest,
    ) )
    assert len( connected_session._unconfirmed ) == 0
    assert subject.state == session.ConfirmableState.CONFIRMED
    assert subject.confirmed.done()

    async with asyncio.timeout( 2 ):
        await publish_task

