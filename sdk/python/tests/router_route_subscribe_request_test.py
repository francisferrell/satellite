# mypy: allow-untyped-defs

import asyncio

import pytest

from satellite import common



@pytest.mark.asyncio
async def test_route_subscribe_request( app_session, router, uri, result ):
    request = common.SubscribeRequest(
        uri = uri,
    )

    route_task = asyncio.create_task( router.route( request, app_session ) )

    args = await router._store.put_subscription_args()
    assert args[0] is app_session
    assert args[1] == request.uri

    message = await route_task
    assert message.request_type == common.ConfirmableMessageType.SubscribeRequest
    assert message.uri == request.uri
    assert message.ref == request.ref

