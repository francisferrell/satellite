# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_subscribe_request( subscribe_request_raw ):
    message = common.Message.fromjson( json.dumps( subscribe_request_raw ) )
    assert isinstance( message, common.SubscribeRequest )
    assert message.uri == subscribe_request_raw['uri']
    assert message.ref == subscribe_request_raw['ref']



@pytest.mark.parametrize( 'required_field', [ 'uri' ] )
def test_subscribe_request_required_fields( subscribe_request_raw, required_field ):
    subscribe_request_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( subscribe_request_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

