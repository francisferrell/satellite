# mypy: allow-untyped-defs

import asyncio
from uuid import uuid4

import pytest

from satellite import common, exceptions
from satellite.app import session



@pytest.mark.asyncio
async def test_call( connected_session, uri, kwargs ):
    assert len( connected_session._unconfirmed ) == 0

    call_task = asyncio.create_task( connected_session.call( uri, kwargs ) )
    message = await connected_session._transport.sent_message()

    assert isinstance( message, common.CallRequest )
    assert message.uri == uri
    assert message.kwargs == kwargs
    call_ref = message.ref

    subject = connected_session._unconfirmed[call_ref]
    assert subject.uri == uri
    assert subject.ref == call_ref
    assert subject.request_type == common.ConfirmableMessageType.CallRequest
    assert subject.state == session.ConfirmableState.REQUESTED

    await connected_session.handle_message( common.Confirmation(
        uri = subject.uri,
        ref = subject.ref,
        request_type = common.ConfirmableMessageType.CallRequest,
    ) )
    assert subject.state == session.ConfirmableState.CONFIRMED
    assert len( connected_session._unconfirmed ) == 0

    assert not call_task.done()

    invocation_result = str( uuid4() )
    await connected_session.handle_message( common.CallResponse(
        uri = uri,
        ref = call_ref,
        result = invocation_result,
    ) )

    async with asyncio.timeout( 2 ):
        assert await call_task == invocation_result



@pytest.mark.asyncio
async def test_call_twice( connected_session, uri, kwargs ):
    call1_task = asyncio.create_task( connected_session.call( uri, kwargs ) )
    call1_request = await connected_session._transport.sent_message()
    call1_ref = call1_request.ref

    call2_task = asyncio.create_task( connected_session.call( uri, kwargs ) )
    call2_request = await connected_session._transport.sent_message()
    call2_ref = call2_request.ref

    assert call1_ref in connected_session._calls
    assert call1_ref in connected_session._unconfirmed
    assert call2_ref in connected_session._calls
    assert call2_ref in connected_session._unconfirmed

    await connected_session.handle_message( common.Confirmation(
        uri = uri,
        ref = call1_request.ref,
        request_type = common.ConfirmableMessageType.CallRequest,
    ) )

    assert call1_ref in connected_session._calls
    assert call1_ref not in connected_session._unconfirmed
    assert call2_ref in connected_session._calls
    assert call2_ref in connected_session._unconfirmed

    await connected_session.handle_message( common.Confirmation(
        uri = uri,
        ref = call2_request.ref,
        request_type = common.ConfirmableMessageType.CallRequest,
    ) )

    assert call1_ref in connected_session._calls
    assert call1_ref not in connected_session._unconfirmed
    assert call2_ref in connected_session._calls
    assert call2_ref not in connected_session._unconfirmed

    result1 = str( uuid4() )
    result2 = str( uuid4() )
    assert result1 != result2

    await connected_session.handle_message( common.CallResponse(
        uri = uri,
        ref = call1_ref,
        result = result1,
    ) )

    assert call1_ref not in connected_session._calls
    assert call1_ref not in connected_session._unconfirmed
    assert call2_ref in connected_session._calls
    assert call2_ref not in connected_session._unconfirmed

    await connected_session.handle_message( common.CallResponse(
        uri = uri,
        ref = call2_ref,
        result = result2,
    ) )

    assert call1_ref not in connected_session._calls
    assert call1_ref not in connected_session._unconfirmed
    assert call2_ref not in connected_session._calls
    assert call2_ref not in connected_session._unconfirmed

    async with asyncio.timeout( 2 ):
        assert await call1_task == result1
        assert await call2_task == result2

