# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_call_response( call_response_raw ):
    message = common.Message.fromjson( json.dumps( call_response_raw ) )
    assert isinstance( message, common.CallResponse )
    assert message.uri == call_response_raw['uri']
    assert message.ref == call_response_raw['ref']
    assert message.result == call_response_raw['result']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'result' ] )
def test_call_response_required_fields( call_response_raw, required_field ):
    call_response_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( call_response_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

