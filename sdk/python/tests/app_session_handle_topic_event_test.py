# mypy: allow-untyped-defs

import asyncio

import pytest

from satellite import common
from satellite.app import session



@pytest.mark.asyncio
async def test_topic_event_unexpected( connected_session, uri, kwargs ):
    topic_event = common.TopicEvent(
        uri = uri,
        kwargs = kwargs,
    )
    assert topic_event.uri not in connected_session._subscriptions
    await connected_session.handle_message( topic_event )



@pytest.mark.asyncio
async def test_topic_event_unhandled_exceptions( mocker, connected_session, uri, kwargs ):
    handler1 = mocker.AsyncMock( side_effect = ZeroDivisionError )
    handler2 = mocker.AsyncMock( side_effect = KeyError )

    subscribe_task = asyncio.create_task( connected_session.subscribe( uri, handler1 ) )
    subscribe_request = await connected_session._transport.sent_message()
    await connected_session.handle_message( common.Confirmation(
        uri = subscribe_request.uri,
        ref = subscribe_request.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    async with asyncio.timeout( 2 ):
        await subscribe_task

    # no request will be sent for this one
    await connected_session.subscribe( uri, handler2 )

    await connected_session.handle_message( common.TopicEvent(
        uri = uri,
        kwargs = kwargs,
    ) )

    handler1.assert_awaited_once_with( kwargs )
    handler2.assert_awaited_once_with( kwargs )

