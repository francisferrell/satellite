# mypy: allow-untyped-defs

import asyncio
from base64 import b64encode
from unittest.mock import AsyncMock
from uuid import uuid4

import pytest

from satellite import common, exceptions
from satellite.app.session import Session
from satellite.app.transport import Transport
from satellite.router import Router
from satellite.router.store import AppSession, CallContext, Store



class MockTransport( Transport ):

    def __init__( self ) -> None:
        self.send_event = asyncio.Event()
        self.mock_send = AsyncMock()

    async def send( self, *args, **kwargs ):
        r = await self.mock_send( *args, **kwargs )
        self.send_event.set()
        return r

    async def recv( self, *args, **kwargs ):
        return ''

    async def close( self, *args, **kwargs ):
        pass

    def _reset_send( self ) -> None:
        self.send_event.clear()
        self.mock_send.reset_mock()

    async def sent_message( self, timeout = 2 ):
        async with asyncio.timeout( timeout ):
            await self.send_event.wait()
        message = common.Message.fromjson( self.mock_send.call_args[0][0] )
        self._reset_send()
        return message



class MockAppSession( AppSession ):

    def __init__( self, *args, **kwargs ) -> None: # type: ignore
        super().__init__( *args, **kwargs )
        self.send_event = asyncio.Event()
        self.mock_send = AsyncMock()

    async def send( self, *args, **kwargs ):
        r = await self.mock_send( *args, **kwargs )
        self.send_event.set()
        return r

    def _reset_send( self ) -> None:
        self.send_event.clear()
        self.mock_send.reset_mock()

    async def sent_message( self, timeout = 2 ):
        async with asyncio.timeout( timeout ):
            await self.send_event.wait()
        message = common.Message.fromjson( self.mock_send.call_args[0][0] )
        self._reset_send()
        return message



class MockStore( Store ):

    def __init__( self, *args, **kwargs ) -> None: # type: ignore
        super().__init__( *args, **kwargs )

        self.put_subscription_event = asyncio.Event()
        self.put_subscription_mock = AsyncMock()

        self.put_registration_event = asyncio.Event()
        self.put_registration_mock = AsyncMock()

        self.put_call_event = asyncio.Event()
        self.put_call_mock = AsyncMock()

        self.delete_call_event = asyncio.Event()
        self.delete_call_mock = AsyncMock()

    async def get_user( self, *args, **kwargs ):
        pass

    async def put_session( self, *args, **kwargs ):
        pass

    async def delete_session( self, *args, **kwargs ):
        pass



    async def put_subscription( self, *args, **kwargs ):
        r = await self.put_subscription_mock( *args, **kwargs )
        self.put_subscription_event.set()
        return r

    def _reset_put_subscription( self ) -> None:
        self.put_subscription_event.clear()
        self.put_subscription_mock.reset_mock()

    async def put_subscription_args( self, timeout = 2 ):
        async with asyncio.timeout( timeout ):
            await self.put_subscription_event.wait()
        args = self.put_subscription_mock.call_args[0]
        self._reset_put_subscription()
        return args



    async def get_subscribers( self, realm: str, uri: str ) -> list[AppSession]:
        return []



    async def put_registration( self, *args, **kwargs ):
        r = await self.put_registration_mock( *args, **kwargs )
        self.put_registration_event.set()
        return r

    def _reset_put_registration( self ) -> None:
        self.put_registration_event.clear()
        self.put_registration_mock.reset_mock()

    async def put_registration_args( self, timeout = 2 ):
        async with asyncio.timeout( timeout ):
            await self.put_registration_event.wait()
        args = self.put_registration_mock.call_args[0]
        self._reset_put_registration()
        return args



    async def get_registrant( self, realm: str, uri: str ) -> AppSession:
        raise exceptions.MissingRegistration()



    async def put_call( self, *args, **kwargs ):
        r = await self.put_call_mock( *args, **kwargs )
        self.put_call_event.set()
        return r

    def _reset_put_call( self ) -> None:
        self.put_call_event.clear()
        self.put_call_mock.reset_mock()

    async def put_call_args( self, timeout = 2 ):
        async with asyncio.timeout( timeout ):
            await self.put_call_event.wait()
        args = self.put_call_mock.call_args[0]
        self._reset_put_call()
        return args



    async def get_call( self, realm: str, uri: str, router_ref: str ) -> CallContext:
        raise exceptions.MissingInvocation()



    async def delete_call( self, *args, **kwargs ):
        r = await self.delete_call_mock( *args, **kwargs )
        self.delete_call_event.set()
        return r

    def _reset_delete_call( self ) -> None:
        self.delete_call_event.clear()
        self.delete_call_mock.reset_mock()

    async def delete_call_args( self, timeout = 2 ):
        async with asyncio.timeout( timeout ):
            await self.delete_call_event.wait()
        args = self.delete_call_mock.call_args[0]
        self._reset_delete_call()
        return args



@pytest.fixture
def connected_session():
    return Session( MockTransport() )



@pytest.fixture
def router():
    return Router( MockStore() )



class Helpers:

    @staticmethod
    def new_app_session():
        return MockAppSession( realm = 'realm', session_id = str( uuid4() ), role = 'anonymous' )



@pytest.fixture
def helpers():
    return Helpers



@pytest.fixture
def app_session( helpers ):
    return helpers.new_app_session()



@pytest.fixture
def uri():
    return 'foo.bar'

@pytest.fixture
def another_uri():
    return 'baz.qux'

@pytest.fixture
def kwargs():
    return { 'foo': 42 }

@pytest.fixture
def result():
    return True



@pytest.fixture
def message_raw( uri ):
    return {
        'type': 'Message',
        'ref': common.make_ref(),
        'uri': uri,
    }


@pytest.fixture
def confirmation_raw( message_raw ):
    message_raw.update( {
        'type': 'Confirmation',
        'request_type': 'SubscribeRequest',
    } )
    return message_raw


@pytest.fixture
def rejection_raw( message_raw ):
    message_raw.update( {
        'type': 'Rejection',
        'request_type': 'SubscribeRequest',
        'klass': 'InvalidMessage',
        'args': ['invalid json'],
    } )
    return message_raw


@pytest.fixture
def subscribe_request_raw( message_raw ):
    message_raw.update( {
        'type': 'SubscribeRequest',
    } )
    return message_raw


@pytest.fixture
def publish_request_raw( message_raw, kwargs ):
    message_raw.update( {
        'type': 'PublishRequest',
        'kwargs': kwargs,
    } )
    return message_raw


@pytest.fixture
def topic_event_raw( message_raw, kwargs ):
    message_raw.update( {
        'type': 'TopicEvent',
        'kwargs': kwargs,
    } )
    return message_raw


@pytest.fixture
def register_request_raw( message_raw ):
    message_raw.update( {
        'type': 'RegisterRequest',
    } )
    return message_raw


@pytest.fixture
def call_request_raw( message_raw, kwargs ):
    message_raw.update( {
        'type': 'CallRequest',
        'kwargs': kwargs,
    } )
    return message_raw


@pytest.fixture
def invocation_request_raw( message_raw, kwargs ):
    message_raw.update( {
        'type': 'InvocationRequest',
        'kwargs': kwargs,
    } )
    return message_raw


@pytest.fixture
def invocation_response_raw( message_raw, result ):
    message_raw.update( {
        'type': 'InvocationResponse',
        'result': result,
    } )
    return message_raw


@pytest.fixture
def call_response_raw( message_raw, result ):
    message_raw.update( {
        'type': 'CallResponse',
        'result':  result,
    } )
    return message_raw

