# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_invalid_json():
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( '{' )
    assert 'invalid json' in str( excinfo.value )



def test_missing_type():
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( '{"ref":"foo","uri":"foo"}' )
    assert 'missing required field: type' in str( excinfo.value )



def test_invalid_type():
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( '{"type":"NotAValidType"}' )
    assert 'invalid type' in str( excinfo.value )



def test_message( message_raw ):
    message = common.Message.fromjson( json.dumps( message_raw ) )
    assert isinstance( message, common.Message )
    assert message.uri == message_raw['uri']
    assert message.ref == message_raw['ref']



def test_message_unique_ref( uri ):
    message1 = common.Message( uri = uri )
    message2 = common.Message( uri = uri )
    assert message1.ref != message2.ref



BAD_REFS = [
    None,
]

@pytest.mark.parametrize( 'bad_ref', BAD_REFS )
def test_message_invalid_ref( message_raw, bad_ref ):
    message_raw.update( ref = bad_ref )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( message_raw ) )
    assert 'ref' in str( excinfo.value.__cause__ )



BAD_URIS = [
    'Foo',
    '.foo',
    'foo.',
    'foo-bar',
    '/foo',
    '',
    None,
]

@pytest.mark.parametrize( 'bad_uri', BAD_URIS )
def test_message_invalid_uri( message_raw, bad_uri ):
    message_raw.update( uri = bad_uri )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( message_raw ) )
    assert 'uri' in str( excinfo.value.__cause__ )



@pytest.mark.parametrize( 'required_field', [ 'uri' ] )
def test_message_required_fields( message_raw, required_field ):
    message_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( message_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

