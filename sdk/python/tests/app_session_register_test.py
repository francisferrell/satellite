# mypy: allow-untyped-defs

import asyncio
from uuid import uuid4

import pytest

from satellite import common, exceptions
from satellite.app import session



@pytest.mark.asyncio
async def test_register( mocker, connected_session, uri, kwargs ):
    assert len( connected_session._registrations ) == 0

    result = str( uuid4() )
    # the default return value of a Mock is a new mock, which is not json serializable
    # so we must provide one explicitly
    handler = mocker.AsyncMock( return_value = result )

    register_task = asyncio.create_task( connected_session.register( uri, handler ) )
    register_request = await connected_session._transport.sent_message()
    assert isinstance( register_request, common.RegisterRequest )
    assert register_request.uri == uri

    subject = connected_session._registrations[uri]
    assert subject.uri == uri
    assert subject.handler is handler
    assert subject.request_type == common.ConfirmableMessageType.RegisterRequest
    assert subject.state == session.ConfirmableState.REQUESTED
    assert not subject.confirmed.done()
    assert not register_task.done()

    await connected_session.handle_message( common.Confirmation(
        uri = subject.uri,
        ref = subject.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    assert subject.state == session.ConfirmableState.CONFIRMED
    assert subject.confirmed.done()

    async with asyncio.timeout( 2 ):
        await register_task

    connected_session._transport.mock_send.reset_mock()
    handler.assert_not_called()
    invocation_ref = common.make_ref()
    await connected_session.handle_message( common.InvocationRequest(
        uri = uri,
        ref = invocation_ref,
        kwargs = kwargs,
    ) )
    handler.assert_awaited_once_with( kwargs )

    invocation_response = await connected_session._transport.sent_message()
    assert isinstance( invocation_response, common.InvocationResponse )
    assert invocation_response.uri == uri
    assert invocation_response.ref == invocation_ref
    assert invocation_response.result == result

    subject = connected_session._unconfirmed[invocation_ref]
    assert subject.uri == uri
    assert subject.ref == invocation_ref
    assert subject.request_type == common.ConfirmableMessageType.InvocationResponse
    assert subject.state == session.ConfirmableState.REQUESTED

    await connected_session.handle_message( common.Confirmation(
        uri = uri,
        ref = invocation_ref,
        request_type = common.ConfirmableMessageType.InvocationResponse,
    ) )
    assert subject.state == session.ConfirmableState.CONFIRMED
    assert invocation_ref not in connected_session._unconfirmed



@pytest.mark.asyncio
async def test_register_sync_handler( mocker, connected_session, uri, kwargs ):
    handler = mocker.MagicMock( return_value = True )

    register_task = asyncio.create_task( connected_session.register( uri, handler ) )
    register_request = await connected_session._transport.sent_message()

    await connected_session.handle_message( common.Confirmation(
        uri = register_request.uri,
        ref = register_request.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    await register_task

    await connected_session.handle_message( common.InvocationRequest(
        uri = uri,
        kwargs = kwargs,
    ) )
    handler.assert_called_once_with( kwargs )



@pytest.mark.asyncio
async def test_register_duplicate( mocker, connected_session, uri ):
    register_task = asyncio.create_task( connected_session.register( uri, lambda kwargs: True ) )
    register_request = await connected_session._transport.sent_message()

    await connected_session.handle_message( common.Confirmation(
        uri = register_request.uri,
        ref = register_request.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    await register_task

    assert uri in connected_session._registrations

    async with asyncio.timeout( 2 ):
        with pytest.raises( exceptions.DuplicateRegistration ):
            await connected_session.register( uri, lambda kwargs: True )



@pytest.mark.asyncio
async def test_register_mismatch_uri( mocker, connected_session, uri, another_uri, kwargs ):
    handler = mocker.AsyncMock( return_value = True )

    register_task = asyncio.create_task( connected_session.register( uri, handler ) )
    register_request = await connected_session._transport.sent_message()

    await connected_session.handle_message( common.Confirmation(
        uri = register_request.uri,
        ref = register_request.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    await register_task

    await connected_session.handle_message( common.InvocationRequest(
        uri = another_uri,
        kwargs = kwargs,
    ) )

    handler.assert_not_called()



@pytest.mark.asyncio
async def test_register_multiple_rpc( mocker, connected_session, uri, another_uri, kwargs ):
    handler1 = mocker.AsyncMock( return_value = True )
    handler2 = mocker.AsyncMock( return_value = True )

    register1_task = asyncio.create_task( connected_session.register( uri, handler1 ) )
    register1_request = await connected_session._transport.sent_message()

    register2_task = asyncio.create_task( connected_session.register( another_uri, handler2 ) )
    register2_request = await connected_session._transport.sent_message()

    await connected_session.handle_message( common.Confirmation(
        uri = register1_request.uri,
        ref = register1_request.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    await connected_session.handle_message( common.Confirmation(
        uri = register2_request.uri,
        ref = register2_request.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    await register1_task
    await register2_task

    await connected_session.handle_message( common.InvocationRequest(
        uri = uri,
        kwargs = kwargs,
    ) )

    handler1.assert_awaited_once()
    handler2.assert_not_called()

    handler1.reset_mock()
    handler2.reset_mock()

    await connected_session.handle_message( common.InvocationRequest(
        uri = another_uri,
        kwargs = kwargs,
    ) )

    handler1.assert_not_called()
    handler2.assert_awaited_once()

