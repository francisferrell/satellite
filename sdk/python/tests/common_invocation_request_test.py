# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_invocation_request( invocation_request_raw ):
    message = common.Message.fromjson( json.dumps( invocation_request_raw ) )
    assert isinstance( message, common.InvocationRequest )
    assert message.uri == invocation_request_raw['uri']
    assert message.ref == invocation_request_raw['ref']
    assert message.kwargs == invocation_request_raw['kwargs']



@pytest.mark.parametrize( 'required_field', [ 'uri', 'kwargs' ] )
def test_invocation_request_required_fields( invocation_request_raw, required_field ):
    invocation_request_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( invocation_request_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

