# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



CONFIRMABLE_TYPES = common.ConfirmableMessageType.__members__.keys()

@pytest.mark.parametrize( 'request_type', CONFIRMABLE_TYPES )
def test_confirmation( confirmation_raw, request_type ):
    confirmation_raw.update( request_type = request_type )
    message = common.Message.fromjson( json.dumps( confirmation_raw ) )
    assert isinstance( message, common.Confirmation )
    assert message.uri == confirmation_raw['uri']
    assert message.ref == confirmation_raw['ref']
    assert message.request_type == confirmation_raw['request_type']



NON_CONFIRMABLE_TYPES = set( common.get_message_classes().keys() ) - set( CONFIRMABLE_TYPES )

@pytest.mark.parametrize( 'request_type', NON_CONFIRMABLE_TYPES )
def test_confirmation_invalid_request_type( confirmation_raw, request_type ):
    confirmation_raw.update( request_type = request_type )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( confirmation_raw ) )
    assert 'request_type' in str( excinfo.value.__cause__ )



@pytest.mark.parametrize( 'required_field', [ 'uri', 'request_type' ] )
def test_confirmation_required_fields( confirmation_raw, required_field ):
    confirmation_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( confirmation_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

