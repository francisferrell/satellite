# mypy: allow-untyped-defs

import json

import pytest

from satellite import common
from satellite.exceptions import InvalidMessage



def test_register_request( register_request_raw ):
    message = common.Message.fromjson( json.dumps( register_request_raw ) )
    assert isinstance( message, common.RegisterRequest )
    assert message.uri == register_request_raw['uri']
    assert message.ref == register_request_raw['ref']



@pytest.mark.parametrize( 'required_field', [ 'uri' ] )
def test_register_request_required_fields( register_request_raw, required_field ):
    register_request_raw.pop( required_field )
    with pytest.raises( InvalidMessage ) as excinfo:
        common.Message.fromjson( json.dumps( register_request_raw ) )
    assert required_field in str( excinfo.value.__cause__ )

