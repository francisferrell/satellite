# mypy: allow-untyped-defs

import asyncio

import pytest

from satellite import common
from satellite.app import session



@pytest.mark.asyncio
async def test_subscribe( mocker, connected_session, uri, kwargs ):
    assert len( connected_session._unconfirmed ) == 0
    assert len( connected_session._subscriptions ) == 0
    handler = mocker.MagicMock()

    subscribe_task = asyncio.create_task( connected_session.subscribe( uri, handler ) )
    subscribe_request = await connected_session._transport.sent_message()
    assert isinstance( subscribe_request, common.SubscribeRequest )
    assert subscribe_request.uri == uri

    sub = connected_session._subscriptions[uri]
    assert sub.uri == uri
    assert sub.handlers == set( [ handler ] )
    assert sub.state == session.ConfirmableState.REQUESTED
    assert not sub.confirmed.done()

    assert sub.ref in connected_session._unconfirmed
    await connected_session.handle_message( common.Confirmation(
        uri = sub.uri,
        ref = sub.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    assert sub.state == session.ConfirmableState.CONFIRMED
    assert sub.ref not in connected_session._unconfirmed

    async with asyncio.timeout( 2 ):
        await subscribe_task

    handler.assert_not_called()
    await connected_session.handle_message( common.TopicEvent(
        uri = uri,
        kwargs = kwargs,
    ) )
    handler.assert_called_once_with( kwargs )



@pytest.mark.asyncio
async def test_subscribe_async_handler( mocker, connected_session, uri, kwargs ):
    handler = mocker.AsyncMock()

    subscribe_task = asyncio.create_task( connected_session.subscribe( uri, handler ) )
    subscribe_request = await connected_session._transport.sent_message()
    await connected_session.handle_message( common.Confirmation(
        uri = subscribe_request.uri,
        ref = subscribe_request.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    async with asyncio.timeout( 2 ):
        await subscribe_task

    await connected_session.handle_message( common.TopicEvent(
        uri = uri,
        kwargs = kwargs,
    ) )

    handler.assert_awaited_once_with( kwargs )



@pytest.mark.asyncio
async def test_subscribe_mismatch_uri( mocker, connected_session, uri, another_uri, kwargs ):
    handler = mocker.MagicMock()

    subscribe_task = asyncio.create_task( connected_session.subscribe( uri, handler ) )
    subscribe_request = await connected_session._transport.sent_message()
    await connected_session.handle_message( common.Confirmation(
        uri = subscribe_request.uri,
        ref = subscribe_request.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    async with asyncio.timeout( 2 ):
        await subscribe_task

    await connected_session.handle_message( common.TopicEvent(
        uri = another_uri,
        kwargs = kwargs,
    ) )

    handler.assert_not_called()



@pytest.mark.asyncio
async def test_subscribe_multiple_handlers( mocker, connected_session, uri, kwargs ):
    handler1 = mocker.MagicMock()
    handler2 = mocker.MagicMock()

    subscribe_task = asyncio.create_task( connected_session.subscribe( uri, handler1 ) )
    subscribe_request = await connected_session._transport.sent_message()
    await connected_session.handle_message( common.Confirmation(
        uri = subscribe_request.uri,
        ref = subscribe_request.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    async with asyncio.timeout( 2 ):
        await subscribe_task

    await connected_session.subscribe( uri, handler2 )

    handler1.assert_not_called()
    handler2.assert_not_called()
    await connected_session.handle_message( common.TopicEvent(
        uri = uri,
        kwargs = kwargs,
    ) )
    handler1.assert_called_once_with( kwargs )
    handler2.assert_called_once_with( kwargs )



@pytest.mark.asyncio
async def test_subscribe_multiple_topics( mocker, connected_session, uri, another_uri, kwargs ):
    handler1 = mocker.MagicMock()
    handler2 = mocker.MagicMock()

    subscribe_task = asyncio.create_task( connected_session.subscribe( uri, handler1 ) )
    subscribe_request = await connected_session._transport.sent_message()
    await connected_session.handle_message( common.Confirmation(
        uri = subscribe_request.uri,
        ref = subscribe_request.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    async with asyncio.timeout( 2 ):
        await subscribe_task

    subscribe_task = asyncio.create_task( connected_session.subscribe( another_uri, handler2 ) )
    subscribe_request = await connected_session._transport.sent_message()
    await connected_session.handle_message( common.Confirmation(
        uri = subscribe_request.uri,
        ref = subscribe_request.ref,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    ) )
    async with asyncio.timeout( 2 ):
        await subscribe_task

    await connected_session.handle_message( common.TopicEvent(
        uri = uri,
        kwargs = kwargs,
    ) )

    handler1.assert_called_once()
    handler2.assert_not_called()

    handler1.reset_mock()
    handler2.reset_mock()

    await connected_session.handle_message( common.TopicEvent(
        uri = another_uri,
        kwargs = kwargs,
    ) )

    handler1.assert_not_called()
    handler2.assert_called_once()

