# mypy: allow-untyped-defs

import asyncio
from unittest.mock import AsyncMock

import pytest

from satellite import common



@pytest.mark.asyncio
async def test_route_register_request( app_session, router, uri, helpers ):
    router._store.put_registration_mock = AsyncMock( return_value = None )

    request = common.RegisterRequest(
        uri = uri,
    )

    route_task = asyncio.create_task( router.route( request, app_session ) )

    args = await router._store.put_registration_args()
    assert args[0] is app_session
    assert args[1] == request.uri

    confirmation = await route_task
    assert isinstance( confirmation, common.Confirmation )
    assert confirmation.request_type == common.ConfirmableMessageType.RegisterRequest
    assert confirmation.uri == request.uri
    assert confirmation.ref == request.ref



@pytest.mark.asyncio
async def test_route_register_request_duplicate( helpers, app_session, router, uri ):
    registrant = helpers.new_app_session()
    router._store.get_registrant = AsyncMock( return_value = registrant )

    request = common.RegisterRequest(
        uri = uri,
    )

    rejection = await router.route( request, app_session )
    assert isinstance( rejection, common.Rejection )
    assert rejection.request_type == common.ConfirmableMessageType.RegisterRequest
    assert rejection.uri == request.uri
    assert rejection.ref == request.ref
    assert rejection.klass == 'DuplicateRegistration'
    assert 'already registered' in str( rejection.args[0] )
    assert registrant.session_id not in str( rejection.args[0] )

    router._store.put_registration_mock.assert_not_awaited()

