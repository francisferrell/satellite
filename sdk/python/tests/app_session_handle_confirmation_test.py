# mypy: allow-untyped-defs

import pytest

from satellite import common
from satellite.app import session



@pytest.mark.asyncio
async def test_confirmation( connected_session, uri ):
    subject = session.Confirmable(
        request_type = common.ConfirmableMessageType.SubscribeRequest,
        uri = uri,
    )
    connected_session._unconfirmed[subject.ref] = subject

    confirmation = common.Confirmation(
        ref = subject.ref,
        uri = subject.uri,
        request_type = subject.request_type,
    )
    await connected_session.handle_message( confirmation )

    assert subject.ref not in connected_session._unconfirmed
    assert subject.confirmed.done()



@pytest.mark.asyncio
async def test_confirmation_unexpected( connected_session, uri ):
    confirmation = common.Confirmation(
        uri = uri,
        request_type = common.ConfirmableMessageType.SubscribeRequest,
    )
    assert confirmation.ref not in connected_session._unconfirmed
    await connected_session.handle_message( confirmation )



@pytest.mark.asyncio
async def test_confirmation_mismatched_ref( connected_session, uri ):
    subject = session.Confirmable(
        request_type = common.ConfirmableMessageType.SubscribeRequest,
        uri = uri,
    )
    connected_session._unconfirmed[subject.ref] = subject

    confirmation = common.Confirmation(
        uri = subject.uri,
        request_type = subject.request_type,
    )
    await connected_session.handle_message( confirmation )

    assert subject == connected_session._unconfirmed[subject.ref]
    assert not subject.confirmed.done()



@pytest.mark.asyncio
async def test_confirmation_mismatched_request_type( connected_session, uri ):
    subject = session.Confirmable(
        request_type = common.ConfirmableMessageType.SubscribeRequest,
        uri = uri,
    )
    connected_session._unconfirmed[subject.ref] = subject

    confirmation = common.Confirmation(
        ref = subject.ref,
        uri = subject.uri,
        request_type = common.ConfirmableMessageType.PublishRequest,
    )
    await connected_session.handle_message( confirmation )

    assert subject == connected_session._unconfirmed[subject.ref]
    assert not subject.confirmed.done()



@pytest.mark.asyncio
async def test_confirmation_mismatched_uri( connected_session, uri, another_uri ):
    subject = session.Confirmable(
        request_type = common.ConfirmableMessageType.SubscribeRequest,
        uri = uri,
    )
    connected_session._unconfirmed[subject.ref] = subject

    confirmation = common.Confirmation(
        ref = subject.ref,
        uri = another_uri,
        request_type = subject.request_type,
    )
    await connected_session.handle_message( confirmation )

    assert subject == connected_session._unconfirmed[subject.ref]
    assert not subject.confirmed.done()

