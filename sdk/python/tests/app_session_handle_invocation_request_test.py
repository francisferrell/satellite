# mypy: allow-untyped-defs

import asyncio

import pytest

from satellite import common
from satellite.app import session



@pytest.mark.asyncio
async def test_invocation_request_unexpected( connected_session, uri, kwargs ):
    invocation_request = common.InvocationRequest(
        uri = uri,
        kwargs = kwargs,
    )

    await connected_session.handle_message( invocation_request )

    connected_session._transport.mock_send.assert_called_once()
    message = await connected_session._transport.sent_message()
    assert isinstance( message, common.Rejection )
    assert message.uri == invocation_request.uri
    assert message.ref == invocation_request.ref
    assert message.request_type == 'InvocationRequest'
    assert message.klass == 'MissingRegistration'
    assert 'unknown RPC uri' in message.args[0] # type: ignore



@pytest.mark.asyncio
async def test_invocation_request_unhandled_exception( mocker, connected_session, uri, kwargs ):
    handler = mocker.AsyncMock( side_effect = ZeroDivisionError )

    register_task = asyncio.create_task( connected_session.register( uri, handler ) )
    register_request = await connected_session._transport.sent_message()

    await connected_session.handle_message( common.Confirmation(
        uri = register_request.uri,
        ref = register_request.ref,
        request_type = common.ConfirmableMessageType.RegisterRequest,
    ) )
    await register_task

    invocation_request = common.InvocationRequest(
        uri = uri,
        kwargs = kwargs,
    )

    await connected_session.handle_message( invocation_request )
    handler.assert_awaited_once_with( kwargs )

    message = await connected_session._transport.sent_message()
    assert isinstance( message, common.Rejection )
    assert message.uri == invocation_request.uri
    assert message.ref == invocation_request.ref
    assert message.request_type == 'InvocationRequest'
    assert message.klass == 'ApplicationError'
    assert message.args[0] == 'ZeroDivisionError'

