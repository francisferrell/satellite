"""
Satellite exceptions.
"""



class SatelliteError( Exception ):
    """
    Base class for all satellite errors that can be serialized and sent over a websocket,
    to either an application or a router.
    """



class InvalidMessage( SatelliteError ):
    """
    Raised when a message being deserialized is not valid.
    """



class DuplicateRegistration( SatelliteError ):
    """
    Raised when a registration is attempted for an RPC that is already registered.
    """



class MissingRegistration( SatelliteError ):
    """
    Raised when a call/invocation is attempted for an RPC that is not registered.
    """



class MissingInvocation( SatelliteError ):
    """
    Raised when an invocation is attempted for an RPC that has not been called.
    """



class ApplicationError( SatelliteError ):
    """
    Raised when the SDK has to handle an exception raised by application code.
    """



class PrivateError( Exception ):
    """
    Base class for all satellite errors that should remain private within the current
    process, whether application or a router. No instance within this exception hierarchy
    should ever be serialized and sent over a websocket.
    """



class MissingUser( PrivateError ):
    """
    Raised when a user could not be found in the router's store.
    """



class InvalidCredentials( PrivateError ):
    """
    Raised when a user's purported crednetials are not valid.
    """



class InvalidSession( PrivateError ):
    """
    Raised when a session is not valid.
    """



class MissingSession( PrivateError ):
    """
    Raised when a session could not be found in the router's store.
    """

