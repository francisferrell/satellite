"""
Satellite Router.
"""

from satellite.router.router import Router

__all__ = [
    'Router',
]

