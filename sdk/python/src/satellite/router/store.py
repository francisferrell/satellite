"""
Satellite router stores.
"""

from abc import abstractmethod
from dataclasses import dataclass

__all__ = [
    'AppSession',
    'Store',
]



@dataclass( frozen = True )
class AppUser:
    """
    An authorized user
    """

    realm: str
    name: str
    role: str



@dataclass( frozen = True )
class AppSession:
    """
    A connected application session
    """

    realm: str
    session_id: str
    role: str



    @abstractmethod
    async def send( self, data: str ) -> None:
        """
        Send `data` to the application session, awaiting the completion of that write operation.
        """



@dataclass( frozen = True )
class CallContext:
    """
    An AppSession that is waiting for an outstanding call with a given `ref`.
    """

    caller: AppSession
    caller_ref: str



class Store:
    """
    Base class for all store implementations.
    """

    @abstractmethod
    async def get_user( self, realm: str, username: str ) -> AppUser:
        """
        Get the User on `realm`.

        Should raise `satellite.exceptions.MissingUser` if the user is not found in the specified realm.
        """



    @abstractmethod
    async def put_session( self, session: AppSession ) -> None:
        """
        Store the `session`.
        """



    @abstractmethod
    async def delete_session( self, session: AppSession ) -> None:
        """
        Delete the provided `session` and all data associated with it.
        """



    @abstractmethod
    async def put_subscription( self, subscriber: AppSession, uri: str ) -> None:
        """
        Store the subscription by `subscriber` to the topic at `uri` on the subscriber's realm.
        """



    @abstractmethod
    async def get_subscribers( self, realm: str, uri: str ) -> list[AppSession]:
        """
        Get the current subscribers of the topic at `uri` on `realm`.
        """



    @abstractmethod
    async def put_registration( self, registrant: AppSession, uri: str ) -> None:
        """
        Store the registration of `registrant` for the RPC at `uri` on the registrant's realm.
        """



    @abstractmethod
    async def get_registrant( self, realm: str, uri: str ) -> AppSession:
        """
        Get the current registrant of the RPC at `uri` on `realm`.

        Should raise `satellite.exceptions.MissingRegistration` if the RPC is not registered.
        """



    @abstractmethod
    async def put_call( self, caller: AppSession, uri: str, router_ref: str, caller_ref: str ) -> None:
        """
        Store the call by `caller` to the RPC at `uri` on the caller's realm, with the provided refs.
        """



    @abstractmethod
    async def delete_call( self, caller: AppSession, uri: str, router_ref: str ) -> None:
        """
        Delete the call by `caller` to the RPC at `uri` on the caller's realm tracked by the provided
        `router_ref`.
        """



    @abstractmethod
    async def get_call( self, realm: str, uri: str, router_ref: str ) -> CallContext:
        """
        Get the context of the call to the RPC at `uri` on `realm` tracked by the provided `router_ref`.

        Should raise `exceptions.MissingInvocation` if there is no oustanding call with the provided details.
        """

