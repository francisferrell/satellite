"""
Satellite Router.
"""

from logging import getLogger

from satellite import common, exceptions
from satellite.router.store import AppSession, Store

logger = getLogger( __name__ )



class Router:
    """
    Implements message routing logic.
    """

    _store: Store



    def __init__( self, store: Store ) -> None:
        self._store = store



    async def route( self, message: common.Message, sender: AppSession ) -> common.Message:
        """
        Route `message` on behalf of `sender`.
        """
        try:
            logger.debug( 'routing: message=%r: sender=%r', message, sender )
            match message:
                case common.SubscribeRequest():
                    await self.route_subscribe_request( message, sender )
                case common.PublishRequest():
                    await self.route_publish_request( message, sender )
                case common.RegisterRequest():
                    await self.route_register_request( message, sender )
                case common.CallRequest():
                    await self.route_call_request( message, sender )
                case common.InvocationResponse():
                    await self.route_invocation_response( message, sender )
                # TODO handle Rejection
                case _:
                    logger.warning( f'unexpected: {message=}' )

            return common.Confirmation(
                request_type = common.ConfirmableMessageType( type( message ).__name__ ),
                uri = message.uri,
                ref = message.ref,
            )
        except exceptions.SatelliteError as err:
            return common.Rejection(
                ref = message.ref,
                uri = message.uri,
                request_type = type( message ).__name__,
                klass = type( err ).__name__,
                args = list( err.args ),
            )



    async def route_subscribe_request( self, message: common.SubscribeRequest, sender: AppSession ) -> None:
        """
        Route `message` on behalf of `sender`.
        """
        await self._store.put_subscription( sender, message.uri )
        logger.info( f'subscribed {message.uri=}' )



    async def route_publish_request( self, message: common.PublishRequest, sender: AppSession ) -> None:
        """
        Route `message` on behalf of `sender`.
        """
        subscribers = await self._store.get_subscribers( sender.realm, message.uri )
        topic_event = common.TopicEvent(
            uri = message.uri,
            kwargs = message.kwargs,
        )
        payload = topic_event.tojson()

        for subscriber in subscribers:
            try:
                await subscriber.send( payload )
            except Exception as err: # pylint: disable=broad-exception-caught
                logger.error( f'failed to post message: {subscriber=}: {err}' )

        logger.info( f'published to {len(subscribers)} subscribers {message.uri=}' )



    async def route_register_request( self, message: common.RegisterRequest, sender: AppSession ) -> None:
        """
        Route `message` on behalf of `sender`.
        """
        # TODO make this atomic within put_registration()
        try:
            registrant = await self._store.get_registrant( sender.realm, message.uri )
        except exceptions.MissingRegistration:
            pass
        else:
            logger.warning( f'duplicate registration: {message=}: {registrant=}' )
            raise exceptions.DuplicateRegistration( f'RPC is already registered: {message.uri=}' )

        await self._store.put_registration( sender, message.uri )
        logger.info( f'registered {message.uri=}' )



    async def route_call_request( self, message: common.CallRequest, sender: AppSession ) -> None:
        """
        Route `message` on behalf of `sender`.
        """
        try:
            registrant = await self._store.get_registrant( sender.realm, message.uri )
        except exceptions.MissingRegistration:
            logger.warning( f'call to missing registration: {message=}: {sender=}' )
            raise

        invocation_request = common.InvocationRequest(
            uri = message.uri,
            kwargs = message.kwargs,
        )
        await self._store.put_call( sender, message.uri, invocation_request.ref, message.ref )
        await registrant.send( invocation_request.tojson() )
        logger.info( f'initiated call {message.uri=}' )



    async def route_invocation_response(
        self,
        message: common.InvocationResponse,
        sender: AppSession
    ) -> None:
        """
        Route `message` on behalf of `sender`.
        """
        try:
            context = await self._store.get_call( sender.realm, message.uri, message.ref )
        except exceptions.MissingInvocation:
            logger.warning( f'invocation for missing call: {message=}: {sender=}' )
            raise

        caller = context.caller
        caller_ref = context.caller_ref
        call_response = common.CallResponse(
            uri = message.uri,
            result = message.result,
            ref = caller_ref
        )
        await caller.send( call_response.tojson() )
        await self._store.delete_call( sender, message.uri, message.ref )
        logger.info( f'resolved call {message.uri=}' )

