"""
Satellite application components.
"""

from abc import abstractmethod

from satellite.app.session import Session



class Component:
    """
    Base class for satellite components of an application.
    """

    @abstractmethod
    async def after_connected( self, session: Session ) -> None:
        """
        Called by `app.run()` when a new transport is connected.

        The application should perofrm any necessary setup, including pub/sub and call/register operations on
        `session`. When this method is called, `session` is freshly connected and no operations beyond auth
        have been performed.
        """



    async def after_disconnected( self ) -> None:
        """
        Called by `app.run()` when a transport has gone away, either because it was disconnected normally or
        because it was lost.

        The application should perofrm any necessary cleanup, but there is no active session to send messages
        to the router. The default implementation does nothing.
        """

