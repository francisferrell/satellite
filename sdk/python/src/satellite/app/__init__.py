"""
Satellite application runner.
"""

from satellite.app.runner import run

__all__ = [
    'run',
]

