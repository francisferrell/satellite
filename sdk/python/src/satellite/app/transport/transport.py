"""
Satellite application transports.
"""

from abc import abstractmethod
from collections.abc import AsyncIterator
from contextlib import AbstractAsyncContextManager
from types import TracebackType

__all__ = [
    'Transport',
    'TransportAdapter',
]



class Transport( AbstractAsyncContextManager['Transport'] ):
    """
    Base class for all transports.
    """

    async def __aexit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None
    ) -> None:
        """
        Clean up after exiting an `async with` statement. By default this awaits `self.close()`.
        """
        await self.close()



    @abstractmethod
    async def send( self, data: str ) -> None:
        """
        Send `data` over the transport, awaiting the completion of that write operation.
        """



    @abstractmethod
    async def recv( self ) -> str:
        """
        Await and return the next data payload from the transport.
        """



    @abstractmethod
    async def close( self ) -> None:
        """
        Close the transport gracefully. The transport must not be used after awaiting this method.
        """



class TransportAdapter( AsyncIterator[Transport] ):
    """
    Async generator of connected transports.

    Use in `async for` or pass to `await anext()` to receive connected transport(s).
    """

    def __aiter__( self ) -> 'TransportAdapter':
        return self



    @abstractmethod
    async def __anext__( self ) -> Transport:
        """
        Return a newly connected `Transport` instance.
        """

