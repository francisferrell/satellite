"""
Satellite application transport implementation based on the `websockets` library.
"""

from base64 import b64encode
from logging import getLogger

import websockets
import websockets.exceptions

from satellite.app.transport import Transport, TransportAdapter

__all__ = [
    'WebsocketTransport',
    'WebsocketTransportAdapter',
]
logger = getLogger()



class WebsocketTransport( Transport ):
    """
    Websocket connection and transport logic.
    """

    _ws: websockets.WebSocketClientProtocol



    def __init__( self, ws: websockets.WebSocketClientProtocol ) -> None:
        super().__init__()
        self._ws = ws



    async def send( self, data: str ) -> None:
        logger.debug( f'sending: {data=}' )
        await self._ws.send( data )
        logger.debug( 'sent' )



    async def recv( self ) -> str:
        logger.debug( 'receiving' )
        data = await self._ws.recv()
        logger.debug( 'received: data=%r', data )
        if isinstance( data, bytes ):
            data = data.decode()
        return data



    async def close( self ) -> None:
        await self._ws.close()



class WebsocketTransportAdapter( TransportAdapter ): # pylint: disable=too-few-public-methods
    """
    Async iterator that opens a new connection to the websocket endpoint `url`, with http basic auth.
    """

    url: str
    headers: dict[str,str]



    def __init__( self, url: str, username: str, password: str ) -> None:
        self.url = url
        self.headers = {
            'Authorization': 'Basic ' + b64encode( f'{username}:{password}'.encode() ).decode()
        }



    async def __anext__( self ) -> WebsocketTransport:
        logger.info( f'opening new connection: url={self.url!r}' )
        ws = await websockets.connect( self.url, extra_headers = self.headers )
        return WebsocketTransport( ws )

