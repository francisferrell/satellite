"""
Satellite application session.

The session provides the primary API your application components will use.
"""

import asyncio
from collections.abc import Awaitable
from dataclasses import dataclass, field
from enum import Enum
from logging import getLogger

from pydantic.types import JsonValue

from satellite import common, exceptions
from satellite.app.transport import Transport

logger = getLogger( __name__ )



class ConfirmableState( Enum ):
    """
    The possible client-side states of a common.ConfirmableMessageType request.
    """
    PENDING = 1
    REQUESTED = 2
    CONFIRMED = 3



@dataclass
class Confirmable:
    """
    Tracking info for any common.ConfirmableMessageType request.
    """
    request_type: common.ConfirmableMessageType
    uri: str
    ref: str = field( default_factory = common.make_ref )
    state: ConfirmableState = ConfirmableState.PENDING
    confirmed: asyncio.Future[None] = field(
        default_factory = lambda: asyncio.get_running_loop().create_future()
    )



@dataclass
class Subscription( Confirmable ):
    """
    Tracking of active topic subscriptions.
    """
    handlers: set[common.TopicHandler] = field( default_factory = set )



def no_op( kwargs: common.Kwargs ) -> None: # pylint: disable=unused-argument
    """
    Default RPC implementation.
    """



@dataclass
class Registration( Confirmable ):
    """
    Tracking of active RPC registrations.
    """
    # Assigning a default handler isn't what I want, but without it dataclass gives:
    #     TypeError: non-default argument 'handler' follows default argument
    # I don't want to make Confirmable field handling more complicated, so this gets a default
    handler: common.RpcHandler = no_op



@dataclass
class Call( Confirmable ):
    """
    Tracking of outstanding RPC calls.
    """
    result: asyncio.Future[JsonValue] = field(
        default_factory = lambda: asyncio.get_running_loop().create_future()
    )



class Session:
    """
    A satellite session with a connected transport.
    """

    _transport: Transport
    _unconfirmed: dict[str,Confirmable]
    _subscriptions: dict[str,Subscription]
    _registrations: dict[str,Registration]
    _calls: dict[str,Call]



    def __init__( self, transport: Transport ) -> None:
        self._transport = transport
        self._unconfirmed = {}
        self._subscriptions = {}
        self._registrations = {}
        self._calls = {}



    async def handle_message( self, message: common.Message ) -> None:
        """
        Handle any incoming `message`.
        """
        try:
            match message:
                case common.Confirmation():
                    await self.handle_confirmation( message )
                case common.TopicEvent():
                    await self.handle_topic_event( message )
                case common.InvocationRequest():
                    await self.handle_invocation_request( message )
                case common.CallResponse():
                    await self.handle_call_response( message )
                # TODO handle Rejection
                case _:
                    logger.warning( f'unexpected: {message=}' )
        except exceptions.SatelliteError as err:
            rejection = common.Rejection(
                ref = message.ref,
                uri = message.uri,
                request_type = type( message ).__name__,
                klass = type( err ).__name__,
                args = list( err.args ),
            )
            await self._transport.send( rejection.tojson() )



    async def handle_confirmation( self, message: common.Confirmation ) -> None:
        """
        Handle the incoming `Confirmation`.
        """
        try:
            subject = self._unconfirmed[message.ref]
        except KeyError:
            logger.warning( f'unexpected Confirmation: {message=} {self._unconfirmed=}' )
            return

        if message.request_type != subject.request_type:
            logger.warning( f'ignoring mismatched Confirmation type: {message=} {subject=}' )
            return

        if message.uri != subject.uri:
            logger.warning( f'ignoring mismatched Confirmation uri: {message=} {subject=}' )
            return

        self._unconfirmed.pop( message.ref, None )
        subject.state = ConfirmableState.CONFIRMED
        subject.confirmed.set_result( None )
        logger.debug( f'confirmed: {message.request_type}: {message.uri=}' )



    async def handle_topic_event( self, message: common.TopicEvent ) -> None:
        """
        Handle the incoming `TopicEvent`.
        """
        try:
            subscription = self._subscriptions[message.uri]
        except KeyError:
            logger.warning( f'unexpected TopicEvent: {message.uri=}: {self._subscriptions=}' )
            return

        logger.info( f'topic event: {message.uri}: {message.kwargs=}' )

        for handler in subscription.handlers:
            try:
                result = handler( message.kwargs )
                if isinstance( result, Awaitable ):
                    await result
            except: # pylint: disable=bare-except
                logger.exception(
                    f'continuing after unhandled exception: {handler=}: {message=}'
                )



    async def handle_invocation_request( self, message: common.InvocationRequest ) -> None:
        """
        Handle any incoming `InvocationRequest`.
        """
        try:
            registration = self._registrations[message.uri]
        except KeyError:
            logger.warning( f'unexpected InvocationRequest: {message.uri=}: {self._registrations=}' )
            raise exceptions.MissingRegistration( f'unknown RPC uri: {message=}' ) from None

        logger.info( f'invocation request: {message.uri}: {message.kwargs=}' )

        try:
            result = registration.handler( message.kwargs )
            if isinstance( result, Awaitable ):
                result = await result
        except Exception as err:
            logger.exception( f'unhandled during RPC invocation: {registration.handler=}: {message=}' )
            raise exceptions.ApplicationError( type( err ).__name__, str( err ) ) from None

        subject = Confirmable(
            request_type = common.ConfirmableMessageType.InvocationResponse,
            uri = message.uri,
            ref = message.ref,
        )
        request = common.InvocationResponse(
            uri = message.uri,
            ref = message.ref,
            result = result,
        )
        await self._request_confirmable( subject, request )



    async def handle_call_response( self, message: common.CallResponse ) -> None:
        """
        Handle the incoming `CallResponse`.
        """
        try:
            call = self._calls.pop( message.ref )
        except KeyError:
            logger.warning(
                f'unexpected CallResponse: {message=} {self._calls=}'
            )
            return

        logger.info( f'invocation response: {message=}' )
        call.result.set_result( message.result )



    async def _request_confirmable( self, subject: Confirmable, request: common.Message ) -> None:
        """
        Given `subject` and the necessary `request` to be sent on its behalf to the router, add it to `ref`
        tracking, send it to the router, and set its state to `REQUESTED`.
        """
        if subject.state is not ConfirmableState.PENDING:
            logger.warning( f'seemingly re-requesting: {subject=} {request=}' )

        if subject.ref in self._unconfirmed:
            logger.warning( f'duplicate unconfirmed ref: {subject.ref}: {subject=} {request=}' )

        await self._transport.send( request.tojson() )
        self._unconfirmed[subject.ref] = subject
        subject.state = ConfirmableState.REQUESTED



    async def subscribe( self, uri: str, handler: common.TopicHandler ) -> None:
        """
        Subscribe to the topic `uri` with `handler` to receive published events.

        The application may call this method multiple times with the same `uri`. All handlers which are passed
        to this method for a given `uri` will be invoked when a topic event is received. The order of
        invocation is not guaranteed. This method may be called with the same `uri` and the same `handler`
        multiple times; only one subscription for that `handler` will actually be created.

        If this is the first time this method is called on this session instance for `uri`, it will await the
        `SubscribeRequest` being sent on this session's transport. It does not await the corresponding
        `Confirmation` from the router. Subsequent calls to this method on the same session instance for the
        same `uri` will return immediately without sending any message to the router.
        """
        if uri not in self._subscriptions:
            self._subscriptions[uri] = Subscription(
                request_type = common.ConfirmableMessageType.SubscribeRequest,
                uri = uri,
            )

        subject = self._subscriptions[uri]
        subject.handlers.add( handler )

        if subject.state is ConfirmableState.PENDING:
            request = common.SubscribeRequest(
                uri = subject.uri,
                ref = subject.ref,
            )
            await self._request_confirmable( subject, request )
            await subject.confirmed



    async def publish( self, uri: str, kwargs: common.Kwargs ) -> None:
        """
        Publish `kwargs` to all subscribers of the topic `uri`.

        This method awaits the `PublishRequest` being sent on this session's transport. It does not await the
        corresponding `Confirmation` from the router.
        """
        subject = Confirmable(
            request_type = common.ConfirmableMessageType.PublishRequest,
            uri = uri,
        )
        request = common.PublishRequest(
            uri = subject.uri,
            ref = subject.ref,
            kwargs = kwargs,
        )
        await self._request_confirmable( subject, request )
        await subject.confirmed



    async def register( self, uri: str, handler: common.RpcHandler ) -> None:
        """
        Register the RPC `uri` with `handler` to provide the implementation.

        This method awaits the `RegisterRequest` being sent on this session's transport. It does not await the
        corresponding `Confirmation` from the router.

        Raises `DuplicateRegistration` if the RPC `uri` is already registered, whether for the same `handler`
        or a another one.
        """
        if uri in self._registrations:
            raise exceptions.DuplicateRegistration

        subject = Registration(
            request_type = common.ConfirmableMessageType.RegisterRequest,
            uri = uri,
            handler = handler,
        )
        self._registrations[uri] = subject

        request = common.RegisterRequest(
            uri = subject.uri,
            ref = subject.ref,
        )
        await self._request_confirmable( subject, request )
        await subject.confirmed



    async def call( self, uri: str, kwargs: common.Kwargs ) -> JsonValue:
        """
        Call the RPC `uri` with `kwargs`.

        This method awaits and returns the `result` from the router.
        """
        subject = Call(
            request_type = common.ConfirmableMessageType.CallRequest,
            uri = uri,
        )
        request = common.CallRequest(
            uri = subject.uri,
            ref = subject.ref,
            kwargs = kwargs,
        )
        self._calls[subject.ref] = subject
        await self._request_confirmable( subject, request )
        return await subject.result

