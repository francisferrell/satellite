"""
Satellite message schemas, validation, and serialization.
"""

from collections.abc import Callable
from enum import Enum
from functools import cache
import json
import re
from uuid import uuid4

from pydantic import BaseModel, Field, ValidationError
from pydantic.types import JsonValue

from satellite.exceptions import InvalidMessage

rx_valid_uri = re.compile( r"^([0-9a-z_]+\.)*([0-9a-z_]+)$" )
"""
A valid `uri` is 1 or more dot-separated words, each word being 1 or more characters, using only the
characters `0-9`, `a-z`, and `_` (case sensitive).
"""

type Kwargs = dict[str,JsonValue]
"""
Type alias for the `kwargs` field on `Message` subclasses.
"""

type TopicHandler = Callable[[Kwargs],None]
"""
Each subscription handler must accept one instance of `Kwargs` and return `None`.
"""

type RpcHandler = Callable[[Kwargs],JsonValue]
"""
Each registration handler must accept one instance of `Kwargs` and return a JSON serializable value.
"""



def make_ref() -> str:
    """
    Create and return a new, unique ref value.
    """
    return str( uuid4() )



@cache
def get_message_classes() -> dict[str,type['Message']]:
    """
    Returns a dictionary of `Message` classes. Keys are the string names of the classes and the values are
    the classes themselves.
    """
    return {
        ** { klass.__name__: klass for klass in Message.__subclasses__() },
        # `Message` instances are not intended to be used in normal operation
        # This special case is here to allow unit tests to use `Message.fromjson` to decode
        # instances of class `Message` for test cases shared by all `Message` subclasses.
        ** { 'Message': Message },
    }



class Message( BaseModel ):
    """
    Base class for all serializable message structures.
    """

    ref: str = Field( default_factory = make_ref )
    uri: str = Field( pattern = rx_valid_uri )


    @staticmethod
    def fromjson( data: str ) -> 'Message':
        """
        Returns as instance of a `Message` subclass from the request body `data`.

        Raises `InvalidMessage` if the message is not valid.
        """
        try:
            body = json.loads( data )
        except json.JSONDecodeError as err:
            raise InvalidMessage( 'invalid json' ) from err

        try:
            message_type = body.pop( 'type' )
        except KeyError:
            raise InvalidMessage( 'missing required field: type' ) from None

        try:
            klass = get_message_classes()[message_type]
        except KeyError:
            raise InvalidMessage( f'invalid type: {message_type}' ) from None

        try:
            return klass( **body )
        except ValidationError as err:
            raise InvalidMessage( f'invalid message: {body=}' ) from err



    def tojson( self ) -> str:
        """
        Serialize this message to json.
        """
        raw = dict( self )
        raw.update( { 'type': type( self ).__name__ } )
        return json.dumps( raw )



class ConfirmableMessageType( str, Enum ):
    """
    Enum of every `Message` subclass name which, when received by the router, results in a `Confirmation`
    being sent to the sending application.
    """
    # pylint: disable=invalid-name # "doesn't conform to UPPER_CASE naming style"
    SubscribeRequest = 'SubscribeRequest'
    PublishRequest = 'PublishRequest'
    RegisterRequest = 'RegisterRequest'
    CallRequest = 'CallRequest'
    InvocationResponse = 'InvocationResponse'



class Confirmation( Message ):
    """
    A `Confirmation` is sent by the router to the application to acknowledge that the router received and
    processed a message. Each confirmation message will have the same `ref` and `uri` as the message
    it confirms. The confirmation's `request_type` will match the `type` of the message it confirms.
    """
    request_type: ConfirmableMessageType



class Rejection( Message ):
    """
    A `Rejection` is sent in either direction to indicate that a message was received and understood
    but could not be processed due to some exception.  Each rejection message will have the same `ref` and
    `uri` as the message it rejects. The confirmation's `request_type` will match the `type` of the message
    it rejects.
    """
    request_type: str
    klass: str
    args: list[JsonValue]



class SubscribeRequest( Message ):
    """
    A `SubscribeRequest` is sent by the application to the router to request the application be subscribed
    to the topic at the specified `uri`.
    """



class PublishRequest( Message ):
    """
    A `PublishRequest` is sent by the application to the router to request that the specified `kwargs` be
    published to all subscribers of the specified `uri`.
    """
    kwargs: Kwargs = Field( repr = False )



class TopicEvent( Message ):
    """
    A `TopicEvent` is sent by the router to each application which has a subscription to the specified `uri`.
    The `ref` will always be the empty string.
    """
    ref: str = ''
    kwargs: Kwargs = Field( repr = False )



class RegisterRequest( Message ):
    """
    A `RegisterRequest` is sent by the application to the router to request that the sender be registered as
    the invocation implementation of the sepcified `uri`.
    """



class CallRequest( Message ):
    """
    A `CallRequest` is sent by the application to the router to request that the registered implementation of
    the specified `uri` be invoked and its `result` returned to the sending application.
    """
    kwargs: Kwargs = Field( repr = False )



class InvocationRequest( Message ):
    """
    An `InvocationRequest` is sent by the router to the application which is registered as the implementation
    of the specified `uri` to request that the RPC be invoked and its `result` be returned to the router.
    """
    kwargs: Kwargs = Field( repr = False )



class InvocationResponse( Message ):
    """
    An `InvocationResponse` is sent by the application to the router to provide the requested `result` for the
    RPC at `uri`. The `ref` will be the same as the `InvocationRequest` this message is in response to.
    """
    result: JsonValue = Field( repr = False )



class CallResponse( Message ):
    """
    A `CallResponse` is sent by the router to the application to provide the requested `result` for the RPC
    at `uri`. The `ref` will be the same as the `CallRequest` this message is in response to.
    """
    result: JsonValue = Field( repr = False )

