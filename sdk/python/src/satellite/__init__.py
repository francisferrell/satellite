"""
The python SDK for Satellite.

Satellite is an API bus. It routes RPC and pub/sub messages between applications. Those applications may be
producers of services on the bus by registering RPCs and/or publishing to topics. They may be consumer of
services on the bus by calling RPCs and/or subscribing to topics. They may be both producers and consumers.

Applications connect to a centralized router. The router routes RPC calls and topic events to the correct
application sessions.
"""

from satellite.version import __version__
