
[![pipeline status](https://gitlab.com/francisferrell/satellite/badges/main/pipeline.svg)](https://gitlab.com/francisferrell/satellite/-/commits/main)
[![coverage report](https://gitlab.com/francisferrell/satellite/badges/main/coverage.svg)](https://gitlab.com/francisferrell/satellite/-/commits/main)

# Satellite

Satellite is an API bus supporting both remote procedure call (RPC) and publish/subscribe (pubsub).
Applications connect via websocket to a router on a desired realm. Connected applications can call RPCs
registered by other applications and can register their own methods as RPCs for other applications to call.
They can also subscribe and publish to topics. All RPC and pubsub operations happen within the scope of the
realm that the applications are connected to.

RPCs and topics are addressed using a simple URI scheme. A satellite URI is a string consisting of 1 or more
components separated by dots. Each component may be made up of the characters `a-z`, `0-9`, or the underscore.
URIs are always lower case.


## Development

Don't forget to run `git config core.hooksPath .git-hooks`.


# Credit

Satellite's design is heavily inspired by crossbar and autobahn.

